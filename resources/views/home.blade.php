@extends('adminlte::page')

@section('title', 'M-Keuangan - Dashboard')

@section('content_header')
  <h1>
    Dashboard
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><i class="fas fa-tachometer-alt"></i> Home</li>
  </ol>
@stop

@section('content')
    {{-- Start Box Logo --}}
      {{-- Start Global Dashboard --}}
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fab fa-fw fa-dropbox"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Barang</span>
              <span class="info-box-number">{{ count($barang) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fas fa-fw fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Users</span>
              <span class="info-box-number">{{ count($users) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fas fa-fw fa-truck"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Supplier</span>
              <span class="info-box-number">{{ count($supplier) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      {{-- End Global Dashboard --}}

      {{-- Start --}}
      {{-- <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fas fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Bookmarks</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div> --}}
      {{-- End --}}

      {{-- Start Per Day Dashboard --}}
      <div class="row">
        {{-- Start Transaksi --}}
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>{{ count($transaksi_today) }}</h3>

                <p>Transaksi Hari Ini</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a class="small-box-footer">
                &nbsp;
              </a>
            </div>
          </div>
        {{-- End Transaksi --}}

        {{-- Start Penjualan --}}
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-grey">
              <div class="inner">
                <h3>{{ $penjualanToday }}</h3>

                <p>Penjualan Hari Ini</p>
              </div>
              <div class="icon">
                <i class="fas fa-tag"></i>
              </div>
              <a href="{{ route('penjualan') }}" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
        {{-- End Penjualan --}}

        {{-- Start Pengeluaran --}}
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
              <div class="inner">
                <h3>{{ $pengeluaranToday }}</h3>

                <p>Pengeluaran Hari Ini</p>
              </div>
              <div class="icon">
                <i class="fas fa-money-bill-alt"></i>
              </div>
              <a href="{{ route('pengeluaran') }}" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
        {{-- End Pengeluaran --}}

        {{-- Start Pembelian --}}
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{ $pembelianToday }}</h3>

                <p>Pembelian Hari Ini</p>
              </div>
              <div class="icon">
                <i class="fas fa-credit-card"></i>
              </div>
              <a href="{{ route('pembelian') }}" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
        {{-- End Pembelian --}}
      </div>
      {{-- End Per Day Dashboard --}}

    {{-- End Box Logo --}}

    {{-- Start Chart --}}
      {{-- Start Chart Per Month --}}
        <div class="row">
          <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Grafik Transaksi {{ date('M Y') }}</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="chart">
                  <canvas id="grafikTransaksiBulananLine" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Grafik Transaksi {{ date('M Y') }}</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="chart">
                  <canvas id="grafikTransaksiBulananBar" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      {{-- End Chart Per Month --}}

      {{-- Start Chart Per Year --}}
        <div class="row">
          <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Grafik Transaksi {{ date('Y') }}</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="chart">
                  <canvas id="grafikTransaksiLine" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Grafik Transaksi {{ date('Y') }}</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="chart">
                  <canvas id="grafikTransaksiBar" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      {{-- End Chart Per Year --}}
    {{-- End Chart --}}

    <h2><i class="fas fa-fw fa-calendar-alt"></i> Timeline</h2>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-7 col-xs-12">
          {{-- Start Timeline --}}

          <ul class="timeline">
            {{-- Start PHP Script for Timeline--}}
            @php
            $timelabelTmp = '';
            @endphp
            {{-- End PHP Script --}}

            @foreach ($history_log as $log)

              @if ($timelabelTmp != date('d M. Y',strtotime($log->updated_at)))
                <li class="time-label">
                  <span class="bg-purple">
                    {{ date('d M. Y',strtotime($log->updated_at)) }}
                    @php
                    $timelabelTmp = date('d M. Y',strtotime($log->updated_at))
                    @endphp
                  </span>
                </li>
              @endif
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                @if ($log->tipe == 'new_user')
                  <i class="fas fa-fw fa-user-plus bg-teal"></i>
                @endif
                @if ($log->tipe == 'login')
                  <i class="fas fa-fw fa-sign-in-alt bg-green"></i>
                @endif
                @if ($log->tipe == 'logout')
                  <i class="fas fa-power-off bg-red"></i>
                @endif
                @if ($log->tipe == 'create')
                  <i class="fas fa-plus bg-green"></i>
                @endif
                @if ($log->tipe == 'update')
                  <i class="fas fa-edit bg-blue"></i>
                @endif
                @if ($log->tipe == 'restore')
                  <i class="fas fa-redo bg-sky"></i>
                @endif
                @if ($log->tipe == 'delete')
                  <i class="fas fa-trash-alt bg-orange"></i>
                @endif
                @if ($log->tipe == 'destroy')
                  <i class="fas fa-trash bg-maroon"></i>
                @endif
                @if ($log->tipe == 'destroyAll')
                  <i class="fas fa-times bg-red"></i>
                @endif
                <div class="timeline-item">
                  <span class="time"><i class="fas fa-clock"></i> {{ date('h:i A',strtotime($log->updated_at)) }}</span>

                  <h3 class="timeline-header"><strong>{{ ucfirst(str_replace('_',' ',$log->tipe)) }}</strong></h3>

                  <div class="timeline-body">
                    {!! $log->action !!}
                  </div>
                  @if (!empty($log->table))
                    <div class="timeline-footer">
                      <a href="{{ route($log->table) }}" class="btn btn-info"><i class="fas fa-link"></i> Lihat ke {{ ucfirst(str_replace('_',' ',$log->table)) }}</a>
                    </div>
                  @endif
                </div>
              </li>
            @endforeach
          </ul>
          {{-- End Timeline --}}
          {{ $history_log->render() }}
        </div>
      </div>
    </div>
@stop

@push('js')
  <script type="text/javascript">
    // Start Chart Per Month
      function daysInMonth(month,year) {
        return new Date(year, month, 0).getDate();
      }

      var date = new Date();
      var banyak_tanggal = daysInMonth(date.getMonth() + 1, date.getFullYear());

      var daftar_tanggal = [];

      for(var tanggal = 1; tanggal <= banyak_tanggal; tanggal++){
        daftar_tanggal.push(tanggal);
      }

      var dataPenjualanBulanan;
      @if(!empty($penjualanBulanan))
        dataPenjualanBulanan = [{{ substr(json_encode($penjualanBulanan),1,-1) }}];
      @else
        for(var tanggal = 1; tanggal <= banyak_tanggal; tanggal++){
          dataPenjualanBulanan.push(0);
        }
      @endif

      var dataPengeluaranBulanan;
      @if(!empty($pengeluaranBulanan))
        dataPengeluaranBulanan = [{{ substr(json_encode($pengeluaranBulanan),1,-1) }}];
      @else
        for(var tanggal = 1; tanggal <= banyak_tanggal; tanggal++){
          dataPengeluaranBulanan.push(0);
        }
      @endif

      var dataPembelianBulanan;
      @if(!empty($pembelianBulanan))
        dataPembelianBulanan = [{{ substr(json_encode($pembelianBulanan),1,-1) }}];
      @else
        for(var tanggal = 1; tanggal <= banyak_tanggal; tanggal++){
          dataPembelianBulanan.push(0);
        }
      @endif

      console.log(dataPengeluaranBulanan);

      var chartDataBulanan = {
        labels  : daftar_tanggal,
        datasets: [
          {
            label               : 'Penjualan',
            fillColor           : 'rgba(210, 214, 222, 1)',
            strokeColor         : 'rgba(210, 214, 222, 1)',
            pointColor          : 'rgba(210, 214, 222, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : dataPenjualanBulanan
          },
          {
            label               : 'Pengeluaran',
            fillColor           : 'rgba(60,141,188,0.9)',
            strokeColor         : 'rgba(60,141,188,0.8)',
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : dataPengeluaranBulanan
          },
          {
            label               : 'Pembelian',
            fillColor           : 'rgba(63, 187, 60, 0.9)',
            strokeColor         : 'rgba(63, 187, 60, 0.8)',
            pointColor          : '#3fbb3c',
            pointStrokeColor    : 'rgba(63, 187, 60, 1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(63, 187, 60, 1)',
            data                : dataPembelianBulanan
          }
        ]
      }

      var chartOptionsBulanan = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
      }

      var grafikTransaksiBulananBarCanvas      = $('#grafikTransaksiBulananBar').get(0).getContext('2d');
      var grafikTransaksiBulananBar            = new Chart(grafikTransaksiBulananBarCanvas);
      grafikTransaksiBulananBar.Bar(chartDataBulanan, chartOptionsBulanan);

      var grafikTransaksiBulananLineCanvas     = $('#grafikTransaksiBulananLine').get(0).getContext('2d');
      var grafikTransaksiBulananLine           = new Chart(grafikTransaksiBulananLineCanvas);
      var chartOptionsBulanan                  = chartOptionsBulanan;
      chartOptionsBulanan.datasetFill          = false;
      grafikTransaksiBulananLine.Line(chartDataBulanan, chartOptionsBulanan);
    // End Chart Per Month

    // Start Chart Per Year
      var nama_bulan = [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    ];
      var dataPenjualan;
      @if(!empty($penjualan))
        dataPenjualan = [{{ substr(json_encode($penjualan),1,-1) }}];
      @else
        dataPenjualan = [
          0,0,0,0,0,0,0,0,0,0,0,0
        ];
      @endif

      var dataPengeluaran;
      @if(!empty($pengeluaran))
        dataPengeluaran = [{{ substr(json_encode($pengeluaran),1,-1) }}];
      @else
        dataPengeluaran = [
          0,0,0,0,0,0,0,0,0,0,0,0
        ];
      @endif

      var dataPembelian;
      @if(!empty($pembelian))
        dataPembelian = [{{ substr(json_encode($pembelian),1,-1) }}];
      @else
        dataPembelian = [
          0,0,0,0,0,0,0,0,0,0,0,0
        ];
      @endif

      var chartData = {
        labels  : nama_bulan,
        datasets: [
          {
            label               : 'Penjualan',
            fillColor           : 'rgba(210, 214, 222, 1)',
            strokeColor         : 'rgba(210, 214, 222, 1)',
            pointColor          : 'rgba(210, 214, 222, 1)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : dataPenjualan
          },
          {
            label               : 'Pengeluaran',
            fillColor           : 'rgba(60,141,188,0.9)',
            strokeColor         : 'rgba(60,141,188,0.8)',
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgba(60,141,188,1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(60,141,188,1)',
            data                : dataPengeluaran
          },
          {
            label               : 'Pembelian',
            fillColor           : 'rgba(63, 187, 60, 0.9)',
            strokeColor         : 'rgba(63, 187, 60, 0.8)',
            pointColor          : '#3fbb3c',
            pointStrokeColor    : 'rgba(63, 187, 60, 1)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(63, 187, 60, 1)',
            data                : dataPembelian
          }
        ]
      }

      var chartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
      }

      var grafikTransaksiBarCanvas      = $('#grafikTransaksiBar').get(0).getContext('2d');
      var grafikTransaksiBar            = new Chart(grafikTransaksiBarCanvas);
      grafikTransaksiBar.Bar(chartData, chartOptions);

      var grafikTransaksiLineCanvas     = $('#grafikTransaksiLine').get(0).getContext('2d');
      var grafikTransaksiLine           = new Chart(grafikTransaksiLineCanvas);
      var chartOptions                  = chartOptions;
      chartOptions.datasetFill          = false;
      grafikTransaksiLine.Line(chartData, chartOptions);
    // End Chart Per Year
  </script>
@endpush
