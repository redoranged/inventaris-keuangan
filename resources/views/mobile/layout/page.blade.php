@extends('mobile.layout.master')

@section('onsenui_css')
    <link rel="stylesheet" href="{{ asset('css/mobile.css') }}">
    @stack('css')
    @yield('css')
@stop

@section('body')
  <ons-splitter>
    <ons-splitter-side id="menu-splitter" side="left" width="220px" collapse swipeable>
      <ons-page>
        @each('mobile.layout.partials.menu-item', config('adminlte.menu'), 'item')
      </ons-page>
    </ons-splitter-side>
    <ons-splitter-content id="content">
      @yield('content')
    </ons-splitter-content>
  </ons-splitter>
@endsection

@section('onsenui_js')
    <script type="text/javascript">
      window.fn = {};

      window.fn.open = function() {
      var menu = document.getElementById('menu-splitter');
      menu.open();
      };

      window.fn.load = function(page) {
      var content = document.getElementById('content');
      var menu = document.getElementById('menu-splitter');
      content.load(page)
        .then(menu.close.bind(menu));
      };


      $(document).ready(function(){
        // TOOLTIP
        $('[data-tooltip=true]').tooltip();
      });
      if(window.screen.width > 479){
        window.location = "{{ route('home') }}";
      }
    </script>

    @if(!config('app.custom.mobile'))
    <script>
      if(window.screen.width <= 479){
        window.location = "{{ route('home') }}";
      }
    </script>
    @endif

    @stack('js')
    @yield('js')
@stop
