@extends('mobile.layout.page')

@section('title', 'M-Keuangan - Supplier')

@section('content')
  <ons-page>
    <ons-toolbar>
      <div class="left">
        <ons-toolbar-button onclick="fn.open()">
          <ons-icon icon="md-menu"></ons-icon>
        </ons-toolbar-button>
      </div>
      <div class="center">
        Supplier
      </div>
    </ons-toolbar>
    <ons-tabbar swipeable position="auto">
      <ons-tab page="table" label="Table" icon="fa-table" active-icon="fa-truck" active></ons-tab>
      <ons-tab page="form" label="Form" icon="fa-wpforms" active-icon="fa-truck"></ons-tab>
      @if(config('app.custom.recycle_bin'))
        <ons-tab page="recycle-bin" label="Recycle Bin" icon="fa-recycle" active-icon="fa-truck"></ons-tab>
      @endif
    </ons-tabbar>
  </ons-page>

  {{-- Start Template --}}
    {{-- Start Table --}}
    <template id="table">
      <ons-page id="Table">
        <div class="card" style="margin-top:20px;">
          <table class="table table-bordered" id="tableSupplier">
            <thead style="display:none;">
              <tr>
                <th>No</th>
                <th>Kode Supplier</th>
                <th>Nama Supplier</th>
                <th>Alamat</th>
                <th>Kontak</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($supplier as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Supplier">{{ $item->kode_supplier }}</td>
                  <td data-label="Nama Supplier">{{ $item->nama_supplier }}</td>
                  <td data-label="Alamat">{{ $item->alamat }}</td>
                  <td data-label="Kontak">{{ $item->kontak }}</td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                        <i class="fa fa-fw fa-edit"></i>
                      </button>
                      <a href="{{ route('supplier.destroy', encrypt($item->id_supplier)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </ons-page>
    </template>
    {{-- End Table --}}

    {{-- Start Form --}}
    <template id="form">
      <ons-page id="Form">
        <div style="text-align:center;margin-top:20px;" class="container-fluid">
          <form action="{{ route('supplier.store') }}" name="formTambah" id="formTambah" method="post">
            @csrf
            <div class="form-group">
              <label for="kode_supplier">Kode Supplier</label>
              <input type="text" class="form-control" id="kode_supplier" value="{{old('kode_supplier')}}" name="kode_supplier" placeholder="Kode Supplier" required/>
              @if ($errors->has('kode_supplier'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kode_supplier') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="nama_supplier">Nama Supplier</label>
              <input type="text" class="form-control" id="nama_supplier" value="{{old('nama_supplier')}}" name="nama_supplier" placeholder="Nama Supplier" required/>
              @if ($errors->has('nama_supplier'))
                  <span class="help-block">
                      <strong>{{ $errors->first('nama_supplier') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" id="alamat" value="{{old('alamat')}}" name="alamat" placeholder="Alamat Supplier"></textarea>
              @if ($errors->has('alamat'))
                  <span class="help-block">
                      <strong>{{ $errors->first('alamat') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="kontak">Kontak</label>
              <input type="text" class="form-control" id="kontak" value="{{old('kontak')}}" name="kontak" placeholder="Kontak Supplier (Nomor Telepon atau Email)"/>
              @if ($errors->has('kontak'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kontak') }}</strong>
                  </span>
              @endif
            </div>
            <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
          </form>
        </div>
      </ons-page>
    </template>
    {{-- End Form --}}

    {{-- Start Recycle Bin --}}
      @if(config('app.custom.recycle_bin'))
      <template id="recycle-bin">
        <ons-page id="Recycle">
          <div class="card" style="margin-top:20px;">
            <table class="table table-bordered" id="tableSupplierBin">
              <thead style="display:none;">
                <tr>
                  <th>No</th>
                  <th>Kode Supplier</th>
                  <th>Nama Supplier</th>
                  <th>Alamat</th>
                  <th>Kontak</th>
                  <th>Tanggal Hapus</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($supplier_bin as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Supplier">{{ $item->kode_supplier }}</td>
                  <td data-label="Nama Supplier">{{ $item->nama_supplier }}</td>
                  <td data-label="Alamat">{{ $item->alamat }}</td>
                  <td data-label="Kontak">{{ $item->kontak }}</td>
                  <td data-label="Tanggal Hapus">{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                        <i class="fa fa-fw fa-redo"></i>
                      </button>
                      <button type="button" data-href="{{ route('supplier.binDestroy', encrypt($item->id_supplier)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </ons-page>
      </template>
      @endif
    {{-- End Recycle Bin --}}

  {{-- End Template --}}

  {{-- Start Modal --}}
    <!-- Start Modal Edit -->
      <div class="modal fade" id="supplierEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog" style="z-index:9999999;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Supplier</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('supplier.store') }}" name="formEdit" id="formEdit" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_supplier">Kode Supplier</label>
                  <input type="text" class="form-control" id="kode_supplier" value="{{old('kode_supplier')}}" name="kode_supplier" placeholder="Kode Supplier" required/>
                </div>
                <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <input type="text" class="form-control" id="nama_supplier" value="{{old('nama_supplier')}}" name="nama_supplier" placeholder="Nama Supplier" required/>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{old('alamat')}}" name="alamat" placeholder="Alamat Supplier"></textarea>
                </div>
                <div class="form-group">
                  <label for="kontak">Kontak</label>
                  <input type="text" class="form-control" id="kontak" value="{{old('kontak')}}" name="kontak" placeholder="Kontak Supplier (Nomor Telepon atau Email)"/>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  {{-- End Modal --}}
@endsection

@section('js')
  <script type="text/javascript">
    $(document).ready(function(){

      // MODAL SHOW
      $('.btnEdit').click(function(e){
        var result = $(this).data('result');

        $('#formEdit').attr('action','{{ url("supplier/update") }}/'+result.id_supplier);

        $('#formEdit #kode_supplier').val(result.kode_supplier);
        $('#formEdit #nama_supplier').val(result.nama_supplier);
        $('#formEdit #alamat').html(result.alamat);
        $('#formEdit #kontak').val(result.kontak);

        $('#supplierEdit').modal('show');
        $('.modal-backdrop').hide();
      });

      // UPDATE
      $('#btnUpdate').click(function(e){
        $('#formEdit').submit();
      });
      
      // RESTORE
      $('.btnRestore').click(function(e){
        var result = $(this).data('result');

        $.post('{{ route("supplier.binRestore") }}',{
          _token : "{{ csrf_token() }}",
          id_supplier : result.id_supplier
        },function(res){
          swal({
            title: 'Success!',
            text: 'Berhasil Restore Data Bin Supplier!',
            type: 'success',
            onClose: () => {
              window.location.reload();
            }
          });
        });
      });

      // DESTROY
      $('.destroy-confirm').click(function(e){
        e.preventDefault();
        swal({
          title: 'Menghapus Recycle Bin?',
          text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Delete'
        }).then((result) => {
          if (result.value) {
            $.post($(this).data('href'),{
                _token : '{{ csrf_token() }}'
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Menghapus Data Bin Supplier!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          }
        })
      });

      // DECLARE DATATABLE
        $('#tableSupplier').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableSupplierBin').DataTable();
        @endif
    });
  </script>
@endsection
