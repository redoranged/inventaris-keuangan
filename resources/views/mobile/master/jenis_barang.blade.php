@extends('mobile.layout.page')

@section('title', 'M-Keuangan - Jenis Barang')

@section('content')
  <ons-page>
    <ons-toolbar>
      <div class="left">
        <ons-toolbar-button onclick="fn.open()">
          <ons-icon icon="md-menu"></ons-icon>
        </ons-toolbar-button>
      </div>
      <div class="center">
        Jenis Barang
      </div>
    </ons-toolbar>
    <ons-tabbar swipeable position="auto">
      <ons-tab page="table" label="Table" icon="fa-table" active-icon="fa-store-alt" active></ons-tab>
      <ons-tab page="form" label="Form" icon="fa-wpforms" active-icon="fa-store-alt"></ons-tab>
      @if(config('app.custom.recycle_bin'))
        <ons-tab page="recycle-bin" label="Recycle Bin" icon="fa-recycle" active-icon="fa-store-alt"></ons-tab>
      @endif
    </ons-tabbar>
  </ons-page>

  {{-- Start Template --}}
    {{-- Start Table --}}
    <template id="table">
      <ons-page id="Table">
        <div class="card" style="margin-top:20px;">
          <table class="table table-bordered" id="tableJenisBarang">
            <thead style="display:none;">
              <tr>
                <th>No</th>
                <th>Kode Jenis Barang</th>
                <th>Nama Jenis Barang</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($jenis_barang as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Jenis Barang">{{ $item->kode_jenis_barang }}</td>
                  <td data-label="Nama Jenis Barang">{{ $item->nama_jenis_barang }}</td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                        <i class="fa fa-fw fa-edit"></i>
                      </button>
                      <a href="{{ route('jenis_barang.destroy', encrypt($item->id_jenis_barang)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </ons-page>
    </template>
    {{-- End Table --}}

    {{-- Start Form --}}
    <template id="form">
      <ons-page id="Form">
        <div style="text-align:center;margin-top:20px;" class="container-fluid">
          <form action="{{ route('jenis_barang.store') }}" name="formTambah" id="formTambah" method="post">
            @csrf
            <div class="form-group">
              <label for="kode_jenis_barang">Kode Jenis Barang</label>
              <input type="text" class="form-control" id="kode_jenis_barang" value="{{old('kode_jenis_barang')}}" name="kode_jenis_barang" placeholder="Kode Jenis Barang" required/>
              @if ($errors->has('kode_jenis_barang'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kode_jenis_barang') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group">
              <label for="nama_jenis_barang">Nama Jenis Barang</label>
              <input type="text" class="form-control" id="nama_jenis_barang" value="{{old('nama_jenis_barang')}}" name="nama_jenis_barang" placeholder="Nama Jenis Barang" required/>
              @if ($errors->has('nama_jenis_barang'))
                  <span class="help-block">
                      <strong>{{ $errors->first('nama_jenis_barang') }}</strong>
                  </span>
              @endif
            </div>
            <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
          </form>
        </div>
      </ons-page>
    </template>
    {{-- End Form --}}

    {{-- Start Recycle Bin --}}
      @if(config('app.custom.recycle_bin'))
      <template id="recycle-bin">
        <ons-page id="Recycle">
          <div class="card" style="margin-top:20px;">
            <table class="table table-bordered" id="tableJenisBarangBin">
              <thead style="display:none;">
                <tr>
                  <th>No</th>
                  <th>Kode Jenis Barang</th>
                  <th>Nama Jenis Barang</th>
                  <th>Tanggal Hapus</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($jenis_barang_bin as $item)
                <tr>
                  <td data-label="No">{{ $loop->iteration }}</td>
                  <td data-label="Kode Jenis Barang">{{ $item->kode_jenis_barang }}</td>
                  <td data-label="Nama Jenis Barang">{{ $item->nama_jenis_barang }}</td>
                  <td data-label="Tanggal Hapus">{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                  <td data-label="Aksi">
                    <div class="btn-group btn-group-sm">
                      <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                        <i class="fa fa-fw fa-redo"></i>
                      </button>
                      <button type="button" data-href="{{ route('jenis_barang.binDestroy', encrypt($item->id_jenis_barang)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                        <i class="fa fa-fw fa-trash"></i>
                      </button>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </ons-page>
      </template>
      @endif
    {{-- End Recycle Bin --}}

  {{-- End Template --}}

  {{-- Start Modal --}}
    <!-- Start Modal Edit -->
      <div class="modal fade" id="jenis_barangEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog" style="z-index:9999999;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Jenis Barang</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('jenis_barang.store') }}" name="formEdit" id="formEdit" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_jenis_barang">Kode Jenis Barang</label>
                  <input type="text" class="form-control" id="kode_jenis_barang" value="{{old('kode_jenis_barang')}}" name="kode_jenis_barang" placeholder="Kode Jenis Barang" required/>
                </div>
                <div class="form-group">
                  <label for="nama_jenis_barang">Nama Jenis Barang</label>
                  <input type="text" class="form-control" id="nama_jenis_barang" value="{{old('nama_jenis_barang')}}" name="nama_jenis_barang" placeholder="Nama Jenis Barang" required/>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  {{-- End Modal --}}
@endsection

@section('js')
  <script type="text/javascript">
    $(document).ready(function(){

      // MODAL SHOW
      $('.btnEdit').click(function(e){
        var result = $(this).data('result');

        $('#formEdit').attr('action','{{ url("jenis_barang/update") }}/'+result.id_jenis_barang);

        $('#formEdit #kode_jenis_barang').val(result.kode_jenis_barang);
        $('#formEdit #nama_jenis_barang').val(result.nama_jenis_barang);

        $('#jenis_barangEdit').modal('show');
        $('.modal-backdrop').hide();
      });

      // UPDATE
      $('#btnUpdate').click(function(e){
        $('#formEdit').submit();
      });
      
      // RESTORE
      $('.btnRestore').click(function(e){
        var result = $(this).data('result');

        $.post('{{ route("jenis_barang.binRestore") }}',{
          _token : "{{ csrf_token() }}",
          id_jenis_barang : result.id_jenis_barang
        },function(res){
          swal({
            title: 'Success!',
            text: 'Berhasil Restore Data Bin Jenis Barang!',
            type: 'success',
            onClose: () => {
              window.location.reload();
            }
          });
        });
      });

      // DESTROY
      $('.destroy-confirm').click(function(e){
        e.preventDefault();
        swal({
          title: 'Menghapus Recycle Bin?',
          text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Delete'
        }).then((result) => {
          if (result.value) {
            $.post($(this).data('href'),{
                _token : '{{ csrf_token() }}'
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Menghapus Data Bin Jenis Barang!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          }
        })
      });

      // DECLARE DATATABLE
        $('#tableJenisBarang').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableJenisBarangBin').DataTable();
        @endif
    });
  </script>
@endsection
