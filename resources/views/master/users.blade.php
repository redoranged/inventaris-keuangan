@extends('adminlte::page')

@section('title', 'M-Keuangan - User')

@section('content_header')
  <h1>
    User
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Master</li>
    <li class="active">User</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{ count($users) }}<sup style="font-size: 20px"></sup></h3>

          <p>Users</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-users-cog"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" data-toggle="modal" data-target="#userTambah">
          Tambah User <i class="fa fa-plus-circle"></i>
        </a>
      </div>
    </div>

    @if(config('app.custom.recycle_bin'))
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-grey">
        <div class="inner">
          <h3>{{ count($users_bin) }}<sup style="font-size: 20px"></sup></h3>

          <p>Recycle Bin</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-recycle"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" id="btnBinDestroy">
          Bersihkan Bin <i class="fa fa-trash"></i>
        </a>
      </div>
    </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Table User <span class="badge">{{ count($users) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableUser">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama User</th>
                    <th>Nama Perusahaan</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Kontak</th>
                    <th>Role</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->perusahaan->nama_perusahaan }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->kontak }}</td>
                    <td>{{ ucfirst($item->role) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                          <i class="fa fa-fw fa-edit"></i>
                        </button>
                        <a href="{{ route('users.destroy', encrypt($item->id_user)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if(config('app.custom.recycle_bin'))
  <div class="row">
    <div class="col-md-12">
      <div class="box box-gray collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Table User Bin <span class="badge">{{ count($users_bin) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
        
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableUserBin">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama User</th>
                    <th>Nama Perusahaan</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Kontak</th>
                    <th>Role</th>
                    <th>Tanggal Hapus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users_bin as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->perusahaan->nama_perusahaan }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->kontak }}</td>
                    <td>{{ ucfirst($item->role) }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                          <i class="fa fa-fw fa-redo"></i>
                        </button>
                        <button type="button" data-href="{{ route('users.binDestroy', encrypt($item->id_user)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  <!-- Start Modal -->
    <!-- Start Modal Tambah -->
      <div class="modal fade" id="userTambah" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Tambah User</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('users.store') }}" name="formTambah" id="formTambah" method="post" enctype="multipart/form-data">
              
                @csrf
                <div class="form-group">
                  <label for="name">Nama User</label>
                  <input type="text" class="form-control" id="name" value="{{(old('name') != '') ? old('name') : ''}}" name="name" placeholder="Nama User" required/>
                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div>
                @if(Auth::user()->role == 'admin')
                  <div class="form-group">
                    <label for="id_perusahaan">Perusahaan</label>
                      <select class="form-control select2" id="id_perusahaan" name="id_perusahaan" style="width:100%;" required>
                        <option value="" selected disabled>Pilih Perusahaan</option>
                      @foreach($perusahaan as $item)
                        <option value="{{ $item->id_perusahaan }}" id="id_perusahaan_{{ $item->id_perusahaan }}">{{ $item->nama_perusahaan }}</option>
                      @endforeach
                      </select>

                      @if ($errors->has('id_perusahaan'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_perusahaan') }}</strong>
                          </span>
                      @endif
                  </div>
                @else
                
                  <input type="hidden" name="id_perusahaan" value="{{ Auth::user()->id_perusahaan }}" required/>
                @endif
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : ''}}" name="email" placeholder="Email" required/>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{(old('alamat') != '') ? old('alamat') : ''}}" name="alamat" placeholder="Alamat User">{{(old('alamat') != '') ? old('alamat') : ''}}</textarea>
                  @if ($errors->has('alamat'))
                      <span class="help-block">
                          <strong>{{ $errors->first('alamat') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="kontak">Kontak</label>
                  <input type="text" class="form-control" id="kontak" value="{{(old('kontak') != '') ? old('kontak') : ''}}" name="kontak" placeholder="Kontak" />
                  @if ($errors->has('kontak'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kontak') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="password">Password Baru</label>
                  <input type="text" class="form-control" id="password" value="{{(old('password') != '') ? old('password') : ''}}" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Tambah -->

    <!-- Start Modal Edit -->
      <div class="modal fade" id="userEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit User</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('users.store') }}" name="formEdit" id="formEdit" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="name">Nama User</label>
                  <input type="text" class="form-control" id="name" value="{{(old('name') != '') ? old('name') : ''}}" name="name" placeholder="Nama User" required/>
                  @if ($errors->has('name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div>
                @if(Auth::user()->role == 'admin')
                  <div class="form-group">
                    <label for="id_perusahaan">Perusahaan</label>
                      <select class="form-control select2" id="id_perusahaan_e" name="id_perusahaan" style="width:100%;" required>
                        <option value="" selected disabled>Pilih Perusahaan</option>
                      @foreach($perusahaan as $item)
                        <option value="{{ $item->id_perusahaan }}" id="id_perusahaan_{{ $item->id_perusahaan }}">{{ $item->nama_perusahaan }}</option>
                      @endforeach
                      </select>

                      @if ($errors->has('id_perusahaan'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_perusahaan') }}</strong>
                          </span>
                      @endif
                  </div>
                @else
                
                  <input type="hidden" name="id_perusahaan" value="{{ Auth::user()->id_perusahaan }}" required/>
                @endif
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : ''}}" name="email" placeholder="Email" required/>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{(old('alamat') != '') ? old('alamat') : ''}}" name="alamat" placeholder="Alamat User">{{(old('alamat') != '') ? old('alamat') : ''}}</textarea>
                  @if ($errors->has('alamat'))
                      <span class="help-block">
                          <strong>{{ $errors->first('alamat') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="kontak">Kontak</label>
                  <input type="text" class="form-control" id="kontak" value="{{(old('kontak') != '') ? old('kontak') : ''}}" name="kontak" placeholder="Kontak" />
                  @if ($errors->has('kontak'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kontak') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="password">Password Baru</label>
                  <input type="text" class="form-control" id="password" value="{{(old('password') != '') ? old('password') : ''}}" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  <!-- End Modal -->
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // Select 2
      $('.select2').select2();

      // Input Mask
      $('[data-mask]').inputmask();
      $('[data-input-mask=true]').inputmask({
        alias:"numeric",
        digits:0,
        digitsOptional:false,
        decimalProtect:true,
        groupSeparator:".",
        radixPoint:",",
        radixFocus:true,
        autoGroup:true,
        autoUnmask:true,
        removeMaskOnSubmit:true
      });

      // RANDOM KODE PERUSAHAAN

      function randomString(len, an){
          an = an&&an.toLowerCase();
          var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
          for(;i++<len;){
            var r = Math.random()*(max-min)+min <<0;
            str += String.fromCharCode(r+=r>9?r<36?55:61:48);
          }
          return str;
      }

      $('#randomKodeUser').click(function(){
        $('#formTambah #kode_user').val('P'+randomString(5));
      });

      // START BUTTON ACTION

        // TAMBAH
        $('#btnTambah').click(function(e){
          $('#formTambah').submit();
        });

        // EDIT

          // MODAL SHOW
          $('.btnEdit').click(function(e){
            var result = $(this).data('result');

            $('#formEdit').attr('action','{{ url("users/update") }}/'+result.id);

            $('#formEdit #id_perusahaan_e #id_perusahaan_'+result.id_perusahaan).prop('selected',true).trigger('change.select2');
            $('#formEdit #name').val(result.name);
            $('#formEdit #alamat').html(result.alamat);
            $('#formEdit #kontak').val(result.kontak);
            $('#formEdit #email').val(result.email);
            // $('#formEdit #logo-file').val(result.logo-file);

            $('#userEdit').modal('show');
          });

          // UPDATE
          $('#btnUpdate').click(function(e){
            $('#formEdit').submit();
          });

          // RESTORE
          $('.btnRestore').click(function(e){
            var result = $(this).data('result');

            $.post('{{ route("users.binRestore") }}',{
              _token : "{{ csrf_token() }}",
              id_user : result.id_user
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Restore Data Bin User!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          });

          // DESTROY
          $('.destroy-confirm').click(function(e){
            e.preventDefault();
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post($(this).data('href'),{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Menghapus Data Bin User!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

          // DESTROY ALL BIN
          $('#btnBinDestroy').click(function(e){
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post('{{ route("users.binDestroyAll") }}',{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Membersihkan Recycle Bin User!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });
      // END BUTTON ACTION


      // TOOLTIP
      $('[data-tooltip=true]').tooltip();
      $('[data-tooltip=image]').tooltip({
        animated: 'fade',
        placement: 'top',
        html: true
      });

      // DECLARE DATATABLE
        $('#tableUser').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableUserBin').DataTable();
        @endif
    });

    // Function
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#formTambah #image-preview')
                    .attr('src', e.target.result);
                $('#formEdit #image-preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
@endpush
