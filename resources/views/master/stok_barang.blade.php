@extends('adminlte::page')

@section('title', 'M-Keuangan - Stok Barang')

@section('content_header')
  <h1>
    Stok Barang
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Master</li>
    <li>Barang</li>
    <li class="active">Stok Barang</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{ count($stok_barang) }}<sup style="font-size: 20px"></sup></h3>

          <p>Stok Barang</p>
        </div>
        <div class="icon">
          <i class="fa fa-fw fa-boxes"></i>
        </div>
        <span class="small-box-footer">
          per Satuan Barang
        </span>
      </div>
    </div>

    {{-- @if(config('app.custom.recycle_bin'))
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-grey">
        <div class="inner">
          <h3>{{ count($stok_barang_bin) }}<sup style="font-size: 20px"></sup></h3>

          <p>Recycle Bin</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-recycle"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" id="btnBinDestroy">
          Bersihkan Bin <i class="fa fa-trash"></i>
        </a>
      </div>
    </div>
    @endif --}}
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Table Stok Barang <span class="badge">{{ count($stok_barang) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableStokBarang">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Stok</th>
                    <th>Harga (/Satuan)</th>
                    <th>Total Harga</th>
                    {{-- <th>Aksi</th> --}}
                  </tr>
                </thead>
                <tbody>
                  @foreach($stok_barang as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->harga->barang->kode_barang }}</td>
                    <td>{{ $item->harga->barang->nama_barang }}</td>
                    <td>{{ $item->harga->nama_satuan }}</td>
                    <td>{{ $item->stok }}</td>
                    <td>Rp. {{ $item->harga->harga }}</td>
                    <td>Rp. {{ $item->stok * $item->harga->harga}}</td>
                    {{-- <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                          <i class="fa fa-fw fa-edit"></i>
                        </button>
                        <a href="{{ route('stok_barang.destroy', encrypt($item->id_stok_barang)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </a>
                      </div>
                    </td> --}}
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- @if(config('app.custom.recycle_bin'))
  <div class="row">
    <div class="col-md-12">
      <div class="box box-gray collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Table Stok Barang Bin <span class="badge">{{ count($stok_barang_bin) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableStokBarangBin">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Stok</th>
                    <th>Tanggal Hapus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($stok_barang_bin as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->harga->barang->kode_barang }}</td>
                    <td>{{ $item->harga->barang->nama_barang }}</td>
                    <td>{{ $item->harga->nama_satuan }}</td>
                    <td>{{ $item->stok }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                          <i class="fa fa-fw fa-redo"></i>
                        </button>
                        <button type="button" data-href="{{ route('stok_barang.binDestroy', encrypt($item->id_stok_barang)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif --}}

  <!-- Start Modal -->
    <!-- Start Modal Tambah -->
      <div class="modal fade" id="stok_barangTambah" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Tambah Stok Barang</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('stok_barang.store') }}" name="formTambah" id="formTambah" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_stok_barang">Kode Stok Barang</label>
                  <input type="text" class="form-control" id="kode_stok_barang" value="{{old('kode_stok_barang')}}" name="kode_stok_barang" placeholder="Kode Stok Barang" required/>
                  @if ($errors->has('kode_stok_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kode_stok_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="nama_stok_barang">Nama Stok Barang</label>
                  <input type="text" class="form-control" id="nama_stok_barang" value="{{old('nama_stok_barang')}}" name="nama_stok_barang" placeholder="Nama Stok Barang" required/>
                  @if ($errors->has('nama_stok_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nama_stok_barang') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Tambah -->

    <!-- Start Modal Edit -->
      <div class="modal fade" id="stok_barangEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Stok Barang</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('stok_barang.store') }}" name="formEdit" id="formEdit" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_stok_barang">Kode Stok Barang</label>
                  <input type="text" class="form-control" id="kode_stok_barang" value="{{old('kode_stok_barang')}}" name="kode_stok_barang" placeholder="Kode Stok Barang" required/>
                </div>
                <div class="form-group">
                  <label for="nama_stok_barang">Nama Stok Barang</label>
                  <input type="text" class="form-control" id="nama_stok_barang" value="{{old('nama_stok_barang')}}" name="nama_stok_barang" placeholder="Nama Stok Barang" required/>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  <!-- End Modal -->
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // START BUTTON ACTION

        // TAMBAH
        $('#btnTambah').click(function(e){
          $('#formTambah').submit();
        });

        // EDIT

          // MODAL SHOW
          $('.btnEdit').click(function(e){
            var result = $(this).data('result');

            $('#formEdit').attr('action','{{ url("stok_barang/update") }}/'+result.id_stok_barang);

            $('#formEdit #kode_stok_barang').val(result.kode_stok_barang);
            $('#formEdit #nama_stok_barang').val(result.nama_stok_barang);

            $('#stok_barangEdit').modal('show');
          });

          // UPDATE
          $('#btnUpdate').click(function(e){
            $('#formEdit').submit();
          });

          // RESTORE
          $('.btnRestore').click(function(e){
            var result = $(this).data('result');

            $.post('{{ route("stok_barang.binRestore") }}',{
              _token : "{{ csrf_token() }}",
              id_stok_barang : result.id_stok_barang
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Restore Data Bin Stok Barang!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          });

          // DESTROY
          $('.destroy-confirm').click(function(e){
            e.preventDefault();
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post($(this).data('href'),{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Menghapus Data Bin Stok Barang!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

          // DESTROY ALL BIN
          $('#btnBinDestroy').click(function(e){
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post('{{ route("stok_barang.binDestroyAll") }}',{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Membersihkan Recycle Bin Stok Barang!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });
      // END BUTTON ACTION


      // TOOLTIP
      $('[data-tooltip=true]').tooltip();

      // DECLARE DATATABLE
        $('#tableStokBarang').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableStokBarangBin').DataTable();
        @endif
    });
  </script>
@endpush
