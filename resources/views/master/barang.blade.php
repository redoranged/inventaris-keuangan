@extends('adminlte::page')

@section('title', 'M-Keuangan - Barang')

@section('content_header')
  <h1>
    Barang
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Master</li>
    <li>Barang</li>
    <li class="active">Barang</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{ count($barang) }}<sup style="font-size: 20px"></sup></h3>

          <p>Barang</p>
        </div>
        <div class="icon">
          <i class="fa fa-fw fa-box"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" data-toggle="modal" data-target="#barangTambah">
          Tambah Barang <i class="fa fa-plus-circle"></i>
        </a>
      </div>
    </div>

    @if(config('app.custom.recycle_bin'))
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-grey">
        <div class="inner">
          <h3>{{ count($barang_bin) }}<sup style="font-size: 20px"></sup></h3>

          <p>Recycle Bin</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-recycle"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" id="btnBinDestroy">
          Bersihkan Bin <i class="fa fa-trash"></i>
        </a>
      </div>
    </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Table Barang <span class="badge">{{ count($barang) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableBarang">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Jenis Barang</th>
                    <th>Supplier</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($barang as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_barang }}</td>
                    <td>{{ $item->nama_barang }}</td>
                    <td>{{ $item->jenis_barang->nama_jenis_barang }}</td>
                    <td>{{ $item->supplier->nama_supplier }}</td>
                    <td>
                      <button type="button" class="btn btn-warning btnDetailHarga" data-harga="{{ $item->harga }}">
                        Detail <i class="fas fa-fw fa-list"></i>
                      </button>
                    </td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                          <i class="fa fa-fw fa-edit"></i>
                        </button>
                        <a href="{{ route('barang.destroy', encrypt($item->id_barang)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if(config('app.custom.recycle_bin'))
  <div class="row">
    <div class="col-md-12">
      <div class="box box-gray collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Table Barang Bin <span class="badge">{{ count($barang_bin) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableBarangBin">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Jenis Barang</th>
                    <th>Supplier</th>
                    <th>Harga</th>
                    <th>Tanggal Hapus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($barang_bin as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_barang }}</td>
                    <td>{{ $item->nama_barang }}</td>
                    <td>{{ $item->jenis_barang->nama_jenis_barang }}</td>
                    <td>{{ $item->supplier->nama_supplier }}</td>
                    <td>
                      <button type="button" class="btn btn-warning btnDetailHarga" data-harga="{{ $item->harga }}">
                        Detail <i class="fas fa-fw fa-list"></i>
                      </button>
                    </td>
                    <td>{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                          <i class="fa fa-fw fa-redo"></i>
                        </button>
                        <button type="button" data-href="{{ route('barang.binDestroy', encrypt($item->id_barang)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  <!-- Start Modal -->
    <!-- Start Modal Tambah -->
      <div class="modal fade" id="barangTambah" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Tambah Barang</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('barang.store') }}" name="formTambah" id="formTambah" method="post">
                @csrf

                <div class="form-group">
                  <label for="id_jenis_barang">Jenis Barang</label>
                  <select class="form-control select2" id="id_jenis_barang" name="id_jenis_barang" style="width:100%;">
                    <option value="" selected disabled>Pilih Jenis Barang</option>
                  @foreach($jenis_barang as $item)
                    <option value="{{ $item->id_jenis_barang }}" id="id_jenis_barang_{{ $item->id_jenis_barang }}">{{ $item->nama_jenis_barang }}</option>
                  @endforeach
                  </select>
                  @if ($errors->has('id_jenis_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('id_jenis_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="kode_barang">Kode Barang</label>
                  <input type="text" class="form-control" id="kode_barang" value="{{old('kode_barang')}}" name="kode_barang" placeholder="Kode Barang" required/>
                  @if ($errors->has('kode_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kode_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="nama_barang">Nama Barang</label>
                  <input type="text" class="form-control" id="nama_barang" value="{{old('nama_barang')}}" name="nama_barang" placeholder="Nama Barang" required/>
                  @if ($errors->has('nama_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nama_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="id_supplier">Supplier</label>
                  <select class="form-control select2" id="id_supplier" name="id_supplier" style="width:100%;">
                    <option value="" selected disabled>Pilih Supplier</option>
                  @foreach($supplier as $item)
                    <option value="{{ $item->id_supplier }}" id="id_supplier_{{ $item->id_supplier }}">{{ $item->nama_supplier }}</option>
                  @endforeach
                  </select>
                  @if ($errors->has('id_supplier'))
                      <span class="help-block">
                          <strong>{{ $errors->first('id_supplier') }}</strong>
                      </span>
                  @endif
                </div>

                <!-- Start Daftar Harga -->
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Daftar Harga</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /. box-header -->

                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Harga</th>
                              <th>per(/) Satuan</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="tambahDaftarHarga">
                            <tr>
                              <td>
                                <div class="input-group">
                                  <span class="input-group-addon">Rp.</span>
                                  <input type="text" name="harga[]" class="form-control mask-rupiah" placeholder="Harga" required/>
                                </div>
                              </td>
                              <td>
                                <input type="text" name="nama_satuan[]" placeholder="Nama Satuan" class="form-control" required/>
                              </td>
                              <td>
                                <button type="button" class="btn btn-danger btnKurangDaftarHarga">
                                  <i class="fas fa-minus"></i>
                                </button>
                              </td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="2">
                                <button type="button" class="btn btn-success" id="btnTambahDaftarHarga">
                                  Tambah Harga <i class="fas fa-plus"></i>
                                </button>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Daftar Harga -->
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Tambah -->

    <!-- Start Modal Edit -->
      <div class="modal fade" id="barangEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Barang</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('barang.store') }}" name="formEdit" id="formEdit" method="post">
                @csrf
                <div class="form-group">
                  <label for="id_jenis_barang">Jenis Barang</label>
                  <select class="form-control select2" id="id_jenis_barang_e" name="id_jenis_barang" style="width:100%;">
                    <option value="" selected disabled>Pilih Jenis Barang</option>
                  @foreach($jenis_barang as $item)
                    <option value="{{ $item->id_jenis_barang }}" id="id_jenis_barang_e_{{ $item->id_jenis_barang }}" {{(old('id_jenis_barang') == $item->id_jenis_barang)? 'selected':''}}>{{ $item->nama_jenis_barang }}</option>
                  @endforeach
                  </select>
                  @if ($errors->has('id_jenis_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('id_jenis_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="kode_barang">Kode Barang</label>
                  <input type="text" class="form-control" id="kode_barang" value="{{old('kode_barang')}}" name="kode_barang" placeholder="Kode Barang" required/>
                  @if ($errors->has('kode_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kode_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="nama_barang">Nama Barang</label>
                  <input type="text" class="form-control" id="nama_barang" value="{{old('nama_barang')}}" name="nama_barang" placeholder="Nama Barang" required/>
                  @if ($errors->has('nama_barang'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nama_barang') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="id_supplier">Supplier</label>
                  <select class="form-control select2" id="id_supplier_e" name="id_supplier" style="width:100%;">
                    <option value="" selected disabled>Pilih Supplier</option>
                  @foreach($supplier as $item)
                    <option value="{{ $item->id_supplier }}" id="id_supplier_e_{{ $item->id_supplier }}" {{(old('id_supplier') == $item->id_supplier)? 'selected':''}}>{{ $item->nama_supplier }}</option>
                  @endforeach
                  </select>
                  @if ($errors->has('id_supplier'))
                      <span class="help-block">
                          <strong>{{ $errors->first('id_supplier') }}</strong>
                      </span>
                  @endif
                </div>
                <!-- Start Daftar Harga -->
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Daftar Harga</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /. box-header -->

                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Harga</th>
                              <th>per(/) Satuan</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="tambahDaftarHargaEdit">

                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="2">
                                <button type="button" class="btn btn-success" id="btnTambahDaftarHargaEdit">
                                  Tambah Harga <i class="fas fa-plus"></i>
                                </button>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Daftar Harga -->
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->

    <!-- Start Modal Detail Harga -->
      <div class="modal fade" id="detailHarga" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Detail Harga</h4>
            </div>
            <div class="modal-body">
              <table class="table table-bordered table-hover" id="tableDetailHarga">
                <thead>
                  <tr>
                    <th>Harga</th>
                    <th>per(/) Satuan</th>
                  </tr>
                </thead>
                <tbody id="tbodyDetailHarga">

                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Detail Harga -->
  <!-- End Modal -->
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // START BUTTON ACTION

        // TAMBAH
        $('#btnTambah').click(function(e){
          $('#formTambah').submit();
        });

        // EDIT

          // MODAL SHOW
          $('.btnEdit').click(function(e){
            $('#tambahDaftarHargaEdit').html('');
            $('#formEdit #id_jenis_barang_e option').removeAttr('selected');
            var result = $(this).data('result');

            $('#formEdit').attr('action','{{ url("barang/update") }}/'+result.id_barang);

            $('#formEdit #kode_barang').val(result.kode_barang);
            $('#formEdit #nama_barang').val(result.nama_barang);
            $('#formEdit #id_jenis_barang_e #id_jenis_barang_e_'+result.id_jenis_barang).attr('selected','selected');
            $('#formEdit #id_supplier_e #id_supplier_e_'+result.id_supplier).attr('selected','selected');

            $.each(result.harga,function(i,item){
              $('#tambahDaftarHargaEdit').append(`
                <tr>
                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" name="harga[]" class="form-control mask-rupiah" value="`+item.harga+`" placeholder="Harga" required/>
                    </div>
                  </td>
                  <td>
                    <input type="text" name="nama_satuan[]" placeholder="Nama Satuan" value="`+item.nama_satuan+`" class="form-control" required/>
                  </td>
                  <td>
                    <button type="button" class="btn btn-danger btnKurangDaftarHargaEdit">
                      <i class="fas fa-minus"></i>
                    </button>
                  </td>
                </tr>
              `);

              // KURANG ROW
              $('.btnKurangDaftarHargaEdit').click(function(e){
                if ($(this).closest('tbody').children().length == 1) {
                  swal('Warning!','Minimal harus ada 1 Harga!','warning');
                }else{
                  $(this).closest('tr').remove();
                }
              });

              // INPUT MASK IDR
              Inputmask.extendAliases({
                'IDR': {
                  alias:"numeric",
                  digits:0,
                  digitsOptional:false,
                  decimalProtect:true,
                  groupSeparator:".",
                  radixPoint:",",
                  radixFocus:true,
                  autoGroup:true,
                  autoUnmask:true,
                  removeMaskOnSubmit:true
                }
              });

              $('.mask-rupiah').inputmask("IDR");
            });

            $('#formEdit #id_jenis_barang_e').trigger('change.select2');
            $('#formEdit #id_supplier_e').trigger('change.select2');
            $('#barangEdit').modal('show');
          });

          // UPDATE
          $('#btnUpdate').click(function(e){
            $('#formEdit').submit();
          });

          // RESTORE
          $('.btnRestore').click(function(e){
            var result = $(this).data('result');

            $.post('{{ route("barang.binRestore") }}',{
              _token : "{{ csrf_token() }}",
              id_barang : result.id_barang
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Restore Data Bin Barang!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          });

          // DESTROY
          $('.destroy-confirm').click(function(e){
            e.preventDefault();
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post($(this).data('href'),{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Menghapus Data Bin Barang!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

          // DESTROY ALL BIN
          $('#btnBinDestroy').click(function(e){
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post('{{ route("barang.binDestroyAll") }}',{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Membersihkan Recycle Bin Barang!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

        // FOR DAFTAR HARGA
          // START FORM TAMBAH
            // KURANG ROW
            $('.btnKurangDaftarHarga').click(function(e){
              if ($(this).closest('tbody').children().length == 1) {
                swal('Warning!','Minimal harus ada 1 Harga!','warning');
              }else{
                $(this).closest('tr').remove();
              }
            });
            // TAMBAH
            $('#btnTambahDaftarHarga').click(function(e){
              $('#tambahDaftarHarga').append(`
                <tr>
                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" name="harga[]" class="form-control mask-rupiah" placeholder="Harga" required/>
                    </div>
                  </td>
                  <td>
                    <input type="text" name="nama_satuan[]" placeholder="Nama Satuan" class="form-control" required/>
                  </td>
                  <td>
                    <button type="button" class="btn btn-danger btnKurangDaftarHarga">
                      <i class="fas fa-minus"></i>
                    </button>
                  </td>
                </tr>
              `);

              $('.btnKurangDaftarHarga').click(function(e){
                  if ($(this).closest('tbody').children().length == 1) {
                    swal('Warning!','Minimal harus ada 1 Harga!','warning');
                  }else{
                    $(this).closest('tr').remove();
                  }
              });

              Inputmask.extendAliases({
                'IDR': {
                  alias:"numeric",
                  digits:0,
                  digitsOptional:false,
                  decimalProtect:true,
                  groupSeparator:".",
                  radixPoint:",",
                  radixFocus:true,
                  autoGroup:true,
                  autoUnmask:true,
                  removeMaskOnSubmit:true
                }
              });

              $('.mask-rupiah').inputmask("IDR");
            });
        // END FORM TAMBAH

        // START FORM EDIT
          // KURANG ROW
          $('.btnKurangDaftarHargaEdit').click(function(e){
            if ($(this).closest('tbody').children().length == 1) {
              swal('Warning!','Minimal harus ada 1 Harga!','warning');
            }else{
              $(this).closest('tr').remove();
            }
          });
          // TAMBAH
          $('#btnTambahDaftarHargaEdit').click(function(e){
            $('#tambahDaftarHargaEdit').append(`
              <tr>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="harga[]" class="form-control mask-rupiah" placeholder="Harga" required/>
                  </div>
                </td>
                <td>
                  <input type="text" name="nama_satuan[]" placeholder="Nama Satuan" class="form-control" required/>
                </td>
                <td>
                  <button type="button" class="btn btn-danger btnKurangDaftarHargaEdit">
                    <i class="fas fa-minus"></i>
                  </button>
                </td>
              </tr>
            `);

            $('.btnKurangDaftarHargaEdit').click(function(e){
                if ($(this).closest('tbody').children().length == 1) {
                  swal('Warning!','Minimal harus ada 1 Harga!','warning');
                }else{
                  $(this).closest('tr').remove();
                }
            });

            Inputmask.extendAliases({
              'IDR': {
                alias:"numeric",
                digits:0,
                digitsOptional:false,
                decimalProtect:true,
                groupSeparator:".",
                radixPoint:",",
                radixFocus:true,
                autoGroup:true,
                autoUnmask:true,
                removeMaskOnSubmit:true
              }
            });

            $('.mask-rupiah').inputmask("IDR");
          });
        // END FORM EDIT

        // START DETAIL HARGA
          $('.btnDetailHarga').click(function(e){
            $('#tbodyDetailHarga').html('');
            var harga = $(this).data('harga');

            $.each(harga,function(i,item){
              $('#tbodyDetailHarga').append(`
                <tr>
                  <td style="text-align:left;">Rp. `+item.harga+`</td>
                  <td>`+item.nama_satuan+`</td>
                </tr>
              `);
            });

            $('#detailHarga').modal('show');
          });
        // END DETAIL HARGA
      // END BUTTON ACTION


      // TOOLTIP
      $('[data-tooltip=true]').tooltip();

      //SELECT2
      $('#formTambah #id_jenis_barang').select2();
      $('#formTambah #id_supplier').select2();
      $('#formEdit #id_jenis_barang_e').select2();
      $('#formEdit #id_supplier_e').select2();

      // DECLARE DATATABLE
        $('#tableBarang').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableBarangBin').DataTable();
        @endif
    });
  </script>
@endpush
