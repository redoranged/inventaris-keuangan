@extends('adminlte::page')

@section('title', 'M-Keuangan - Supplier')

@section('content_header')
  <h1>
    Supplier
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Master</li>
    <li class="active">Supplier</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-teal">
        <div class="inner">
          <h3>{{ count($supplier) }}<sup style="font-size: 20px"></sup></h3>

          <p>Supplier</p>
        </div>
        <div class="icon">
          <i class="fa fa-fw fa-truck"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" data-toggle="modal" data-target="#supplierTambah">
          Tambah Supplier <i class="fa fa-plus-circle"></i>
        </a>
      </div>
    </div>

    @if(config('app.custom.recycle_bin'))
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-grey">
        <div class="inner">
          <h3>{{ count($supplier_bin) }}<sup style="font-size: 20px"></sup></h3>

          <p>Recycle Bin</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-recycle"></i>
        </div>
        <a href="javascript:void(0);" class="small-box-footer" id="btnBinDestroy">
          Bersihkan Bin <i class="fa fa-trash"></i>
        </a>
      </div>
    </div>
    @endif
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Table Supplier <span class="badge">{{ count($supplier) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableSupplier">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Supplier</th>
                    <th>Nama Supplier</th>
                    <th>Alamat</th>
                    <th>Kontak</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($supplier as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_supplier }}</td>
                    <td>{{ $item->nama_supplier }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->kontak }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnEdit" data-tooltip="true" data-result="{{ $item }}" title="Edit" data-placement="left">
                          <i class="fa fa-fw fa-edit"></i>
                        </button>
                        <a href="{{ route('supplier.destroy', encrypt($item->id_supplier)) }}" class="btn btn-sm btn-danger" data-tooltip="true" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if(config('app.custom.recycle_bin'))
  <div class="row">
    <div class="col-md-12">
      <div class="box box-gray collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Table Supplier Bin <span class="badge">{{ count($supplier_bin) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableSupplierBin">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Supplier</th>
                    <th>Nama Supplier</th>
                    <th>Alamat</th>
                    <th>Kontak</th>
                    <th>Tanggal Hapus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($supplier_bin as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->kode_supplier }}</td>
                    <td>{{ $item->nama_supplier }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->kontak }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->deleted_at)) }}<br>{{ date('h:i A',strtotime($item->deleted_at)) }}</td>
                    <td>
                      <div class="btn-group btn-group-sm">
                        <button type="button" class="btn btn-sm btn-info btnRestore" data-tooltip="true" data-result="{{ $item }}" title="Restore" data-placement="left">
                          <i class="fa fa-fw fa-redo"></i>
                        </button>
                        <button type="button" data-href="{{ route('supplier.binDestroy', encrypt($item->id_supplier)) }}" class="btn btn-sm btn-danger destroy-confirm" data-tooltip="true" data-result="{{ $item }}" title="Delete" data-placement="right">
                          <i class="fa fa-fw fa-trash"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif

  <!-- Start Modal -->
    <!-- Start Modal Tambah -->
      <div class="modal fade" id="supplierTambah" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Tambah Supplier</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('supplier.store') }}" name="formTambah" id="formTambah" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_supplier">Kode Supplier</label>
                  <input type="text" class="form-control" id="kode_supplier" value="{{old('kode_supplier')}}" name="kode_supplier" placeholder="Kode Supplier" required/>
                  @if ($errors->has('kode_supplier'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kode_supplier') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <input type="text" class="form-control" id="nama_supplier" value="{{old('nama_supplier')}}" name="nama_supplier" placeholder="Nama Supplier" required/>
                  @if ($errors->has('nama_supplier'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nama_supplier') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{old('alamat')}}" name="alamat" placeholder="Alamat Supplier"></textarea>
                  @if ($errors->has('alamat'))
                      <span class="help-block">
                          <strong>{{ $errors->first('alamat') }}</strong>
                      </span>
                  @endif
                </div>
                <div class="form-group">
                  <label for="kontak">Kontak</label>
                  <input type="text" class="form-control" id="kontak" value="{{old('kontak')}}" name="kontak" placeholder="Kontak Supplier (Nomor Telepon atau Email)"/>
                  @if ($errors->has('kontak'))
                      <span class="help-block">
                          <strong>{{ $errors->first('kontak') }}</strong>
                      </span>
                  @endif
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnTambah" class="btn btn-success">TAMBAH</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Tambah -->

    <!-- Start Modal Edit -->
      <div class="modal fade" id="supplierEdit" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="modal-title">Edit Supplier</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('supplier.store') }}" name="formEdit" id="formEdit" method="post">
                @csrf
                <div class="form-group">
                  <label for="kode_supplier">Kode Supplier</label>
                  <input type="text" class="form-control" id="kode_supplier" value="{{old('kode_supplier')}}" name="kode_supplier" placeholder="Kode Supplier" required/>
                </div>
                <div class="form-group">
                  <label for="nama_supplier">Nama Supplier</label>
                  <input type="text" class="form-control" id="nama_supplier" value="{{old('nama_supplier')}}" name="nama_supplier" placeholder="Nama Supplier" required/>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" id="alamat" value="{{old('alamat')}}" name="alamat" placeholder="Alamat Supplier"></textarea>
                </div>
                <div class="form-group">
                  <label for="kontak">Kontak</label>
                  <input type="text" class="form-control" id="kontak" value="{{old('kontak')}}" name="kontak" placeholder="Kontak Supplier (Nomor Telepon atau Email)"/>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btnUpdate" class="btn btn-success">EDIT</button>
            </div>
          </div>
        </div>
      </div>
    <!-- End Modal Edit -->
  <!-- End Modal -->
@stop

@push('js')
  <script type="text/javascript">
    $(document).ready(function(){
      // START BUTTON ACTION

        // TAMBAH
        $('#btnTambah').click(function(e){
          $('#formTambah').submit();
        });

        // EDIT

          // MODAL SHOW
          $('.btnEdit').click(function(e){
            var result = $(this).data('result');

            $('#formEdit').attr('action','{{ url("supplier/update") }}/'+result.id_supplier);

            $('#formEdit #kode_supplier').val(result.kode_supplier);
            $('#formEdit #nama_supplier').val(result.nama_supplier);
            $('#formEdit #alamat').html(result.alamat);
            $('#formEdit #kontak').val(result.kontak);

            $('#supplierEdit').modal('show');
          });

          // UPDATE
          $('#btnUpdate').click(function(e){
            $('#formEdit').submit();
          });

          // RESTORE
          $('.btnRestore').click(function(e){
            var result = $(this).data('result');

            $.post('{{ route("supplier.binRestore") }}',{
              _token : "{{ csrf_token() }}",
              id_supplier : result.id_supplier
            },function(res){
              swal({
                title: 'Success!',
                text: 'Berhasil Restore Data Bin Supplier!',
                type: 'success',
                onClose: () => {
                  window.location.reload();
                }
              });
            });
          });

          // DESTROY
          $('.destroy-confirm').click(function(e){
            e.preventDefault();
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post($(this).data('href'),{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Menghapus Data Bin Supplier!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });

          // DESTROY ALL BIN
          $('#btnBinDestroy').click(function(e){
            swal({
              title: 'Menghapus Recycle Bin?',
              text: "Data yang sudah dihapus di Recycle Bin tidak bisa dikembalikan!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
            }).then((result) => {
              if (result.value) {
                $.post('{{ route("supplier.binDestroyAll") }}',{
                    _token : '{{ csrf_token() }}'
                },function(res){
                  swal({
                    title: 'Success!',
                    text: 'Berhasil Membersihkan Recycle Bin Supplier!',
                    type: 'success',
                    onClose: () => {
                      window.location.reload();
                    }
                  });
                });
              }
            })
          });
      // END BUTTON ACTION


      // TOOLTIP
      $('[data-tooltip=true]').tooltip();

      // DECLARE DATATABLE
        $('#tableSupplier').DataTable();
        @if(config('app.custom.recycle_bin'))
        $('#tableSupplierBin').DataTable();
        @endif
    });
  </script>
@endpush
