@extends('adminlte::page')

@section('title', 'M-Keuangan - Setting')

@section('content_header')
  <h1>
    Setting
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Main</li>
    <li class="active">Setting</li>
  </ol>
@stop

@section('content')
  <div class="row">
    <div class="col-md-3 col-xs-12">
      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <center>
            <i class="fa fa-fw fa-4x fa-user"></i>
          </center>

          <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

          <p class="text-muted text-center">{{ ucfirst(Auth::user()->role) }}</p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <strong>Saldo</strong>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    Rp. {{ number_format(Auth::user()->perusahaan->saldo,0,',','.') }}
                  </div>
                </div>
              </div>
            </li>
          </ul>

          @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
              <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" class="btn btn-danger btn-block">
                  <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
              </a>
          @else
              <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger btn-block">
                  <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
              </a>
              <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                  @if(config('adminlte.logout_method'))
                      {{ method_field(config('adminlte.logout_method')) }}
                  @endif
                  {{ csrf_field() }}
              </form>
          @endif

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <div class="col-md-9 col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#profile" data-toggle="tab">My Profile</a></li>
          @if(Auth::user()->role != 'admin')
            <li><a href="#perusahaan" data-toggle="tab">Perusahaan</a></li>
          @endif
          <li><a href="#change-password" data-toggle="tab">Change Password</a></li>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="profile">
            <form class="form-horizontal" action="{{ route('setting.update',encrypt(Auth::user()->id)) }}" method="post">
              @csrf
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>

                <div class="col-sm-10">
                  <input type="text" name="name" class="form-control" id="inputName" placeholder="Name" value="{{ (!empty(old('name'))) ? old('name') : Auth::user()->name }}" required autofocus>
                </div>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                <div class="col-sm-10">
                  <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ (!empty(old('email'))) ? old('email') : Auth::user()->email }}" readonly>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="inputAlamat" class="col-sm-2 control-label">Alamat</label>

                <div class="col-sm-10">
                  <textarea name="alamat" rows="4" cols="80" class="form-control" id="inputAlamat" placeholder="Alamat">{{ (!empty(old('alamat'))) ? old('alamat') : Auth::user()->alamat }}</textarea>
                </div>
                @if ($errors->has('alamat'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alamat') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="inputKontak" class="col-sm-2 control-label">Kontak</label>

                <div class="col-sm-10">
                  <input type="text" name="kontak" class="form-control" id="inputKontak" placeholder="Kontak (No. Telepon / Email)" value="{{ (!empty(old('kontak'))) ? old('kontak') : Auth::user()->kontak }}" />
                </div>
                @if ($errors->has('kontak'))
                    <span class="help-block">
                        <strong>{{ $errors->first('kontak') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Save</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.tab-pane -->

          @if(Auth::user()->role != 'admin')
            <div class="tab-pane" id="perusahaan">
              <form class="form-horizontal" id="formPerusahaan" action="{{ route('setting.updatePerusahaan',encrypt(Auth::user()->id_perusahaan)) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="kode_perusahaan" class="control-label">Kode Perusahaan</label>

                        <input type="text" name="kode_perusahaan" class="form-control" id="kode_perusahaan" placeholder="Kode Perusahaan" value="{{ (!empty(old('kode_perusahaan'))) ? old('kode_perusahaan') : Auth::user()->perusahaan->kode_perusahaan }}" />
                        @if ($errors->has('kode_perusahaan'))
                          <span class="help-block">
                            <strong>{{ $errors->first('kode_perusahaan') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="nama_perusahaan" class="control-label">Nama Perusahaan</label>

                        <input type="text" name="nama_perusahaan" class="form-control" id="nama_perusahaan" placeholder="Nama Perusahaan" value="{{ (!empty(old('nama_perusahaan'))) ? old('nama_perusahaan') : Auth::user()->perusahaan->nama_perusahaan }}" />
                        @if ($errors->has('nama_perusahaan'))
                          <span class="help-block">
                            <strong>{{ $errors->first('nama_perusahaan') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="control-label">Alamat Perusahaan</label>

                        <textarea name="alamat" class="form-control" id="alamat" placeholder="Alamat Perusahaan">{{ (!empty(old('alamat'))) ? old('alamat') : Auth::user()->perusahaan->alamat }}</textarea>
                        @if ($errors->has('alamat'))
                          <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="no_telepon">No Telepon</label>
                        <input type="text" class="form-control" id="no_telepon" value="{{(old('no_telepon') != '') ? old('no_telepon') : Auth::user()->perusahaan->no_telepon}}" name="no_telepon" placeholder="No Telepon" data-inputmask="'mask': ['9999-9999-99999', '+0999 9999 9999[9]']" data-mask/>
                        @if ($errors->has('no_telepon'))
                          <span class="help-block">
                            <strong>{{ $errors->first('no_telepon') }}</strong>
                          </span>
                        @endif
                      </div>
                      <div class="form-group">
                        <label for="email">Email Perusahaan</label>
                        <input type="email" class="form-control" id="email" value="{{(old('email') != '') ? old('email') : Auth::user()->perusahaan->email}}" name="email" placeholder="Email Perusahaan" required/>
                        @if ($errors->has('email'))
                          <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif
                      </div>
                      <center>
                        <img src="{{(old('logo-file') != '') ? old('logo-file') : Auth::user()->perusahaan->logo}}" id="image-preview" style="max-width:300px;"/>
                      </center>
                      <div class="form-group">
                        <label for="logo-file">Logo Perusahaan</label>
                        <input type="file" id="logo-file" value="{{(old('logo-file') != '') ? old('logo-file') : Auth::user()->perusahaan->logo}}" accept="image/*" name="logo-file" placeholder="Logo Perusahaan" onchange="readURL(this);"/>
                        @if ($errors->has('logo-file'))
                          <span class="help-block">
                            <strong>{{ $errors->first('logo-file') }}</strong>
                          </span>
                        @endif
                      </div>
                      <button type="submit" class="btn btn-success">
                        UPDATE
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          @endif


          <div class="tab-pane" id="change-password">
            <form class="form-horizontal" action="{{ route('setting.updatePassword',encrypt(Auth::user()->id)) }}" method="post">
              @csrf
              <div class="form-group">
                <label for="inputOldPassword" class="col-sm-3 control-label">Old Password</label>

                <div class="col-sm-9">
                  <input type="password" name="oldPassword" class="form-control" id="inputOldPassword" placeholder="Old Password" value="{{ (!empty(old('oldPassword'))) ? old('oldPassword') : Auth::user()->oldPassword }}" required autofocus>
                </div>
                @if ($errors->has('oldPassword'))
                    <span class="help-block">
                        <strong>{{ $errors->first('oldPassword') }}</strong>
                    </span>
                @endif
              </div>
              <hr>
              <div class="form-group">
                <label for="inputNewPassword" class="col-sm-3 control-label">New Password</label>

                <div class="col-sm-9">
                  <input type="password" name="newPassword" class="form-control" id="inputNewPassword" placeholder="New Password" value="{{ (!empty(old('newPassword'))) ? old('newPassword') : Auth::user()->newPassword }}" required>
                </div>
                @if ($errors->has('newPassword'))
                    <span class="help-block">
                        <strong>{{ $errors->first('newPassword') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="inputConfirmNewPassword" class="col-sm-3 control-label">Confirm New Password</label>

                <div class="col-sm-9">
                  <input type="password" name="confirmNewPassword" class="form-control" id="inputConfirmNewPassword" placeholder="Confirm New Password" value="{{ (!empty(old('confirmNewPassword'))) ? old('confirmNewPassword') : Auth::user()->confirmNewPassword }}" required>
                </div>
                @if ($errors->has('confirmNewPassword'))
                    <span class="help-block">
                        <strong>{{ $errors->first('confirmNewPassword') }}</strong>
                    </span>
                @endif
              </div>
              <div class="col-md-offset-3">
                <button type="submit" class="btn btn-info">
                  Change Password
                </button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.tab content -->
      </div>
    </div>
  </div>
@stop

@push('js')
  <script type="text/javascript">
    // Function
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#formPerusahaan #image-preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
@endpush
