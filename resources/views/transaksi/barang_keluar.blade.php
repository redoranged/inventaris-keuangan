@extends('adminlte::page')

@section('title', 'M-Keuangan - Barang Keluar')

@section('content_header')
  <h1>
    Barang Keluar
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Transaksi</li>
    <li>Data Barang</li>
    <li class="active">Barang Keluar</li>
  </ol>
@stop

@section('content')
  {{-- Start Table Barang Keluar --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Barang Keluar <span class="badge">{{ count($transaksi) }}</span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tableBarangKeluar">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Satuan</th>
                    <th>Total Harga</th>
                    <th>User</th>
                    <th>Tanggal Transaksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($transaksi as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->harga->barang->nama_barang }}</td>
                    <td>{{ $item->jumlah_barang }} {{ $item->harga->nama_satuan }}</td>
                    <td>Rp. {{ $item->harga->harga }}</td>
                    <td>Rp. {{ $item->total_harga }}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->created_at)) }}<br>{{ date('h:i A',strtotime($item->created_at)) }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Table Barang Keluar --}}

  {{-- Start Form Tambah --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Barang Keluar</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-offset-2 col-md-8 col-xs-12">
              <form action="{{ route('barang_keluar.store') }}" id="formTambah" method="post">
                @csrf
                <div class="form-group">
                  <label for="id_barang">Nama Barang</label>
                  <select class="form-control select2" id="id_barang" name="id_barang" style="width:100%;">
                    <option value="" selected disabled>Pilih Barang</option>
                  @foreach($barang as $item)
                    <option value="{{ $item->id_barang }}" id="harga_satuan_{{ $item->id_barang }}">{{ $item->nama_barang }}</option>
                  @endforeach
                  </select>
                </div>
                <div id="box-keterangan-barang" class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tentang Barang</h3>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="form-group">
                          <label class="col-md-4" for="kode_barang">Kode Barang </label>
                          <span class="col-md-1">:</span>
                          <span class="col-md-7" id="kode_barang"></span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="form-group">
                          <label class="col-md-4" for="jenis_barang">Jenis Barang </label>
                          <span class="col-md-1">:</span>
                          <span class="col-md-7" id="jenis_barang"></span>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="form-group">
                          <label class="col-md-4" for="supplier">Supplier </label>
                          <span class="col-md-1">:</span>
                          <span class="col-md-7" id="supplier"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="harga_satuan">Harga Satuan Barang</label>
                  <select class="form-control select2" id="harga_satuan" name="harga_satuan" style="width:100%;" disabled>
                    <option value="" selected disabled>Pilih Barang</option>
                  </select>
                  <input type="hidden" name="id_harga" id="id_harga" value="0">
                  <input type="hidden" name="harga" id="harga" value="0"/>
                </div>
                <div class="form-group">
                  <label for="jumlah_barang">Jumlah Barang</label>
                  <input type="text" name="jumlah_barang" id="jumlah_barang" class="form-control" placeholder="Jumlah Barang" data-mask="true" value="0" min-value="0" required/>
                </div>
                <div class="form-group">
                  <label for="total_harga">Total Harga</label>
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="total_harga" id="total_harga" class="form-control" placeholder="Total Harga" data-mask="true" value="0" min-value="0" readonly required/>
                  </div>
                </div>
                <button type="submit" class="btn btn-success">
                  TAMBAH
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Form Tambah --}}
@stop

@push('js')
  <script type="text/javascript">
    function calcTotalHarga(){
      var harga = $('#formTambah #harga').val();
      var jumlah_barang = $('#formTambah #jumlah_barang').val();

      $('#formTambah #total_harga').val(harga*jumlah_barang);

      $('#formTambah #total_harga').inputmask();
    }

    function setHarga(){

      var result = $('#formTambah #harga_satuan').val();
      var value = result.split(".|.");

      $('#formTambah #id_harga').val(value[0]);
      $('#formTambah #harga').val(value[1]);
      calcTotalHarga();
    }

    function setSatuanHarga(){
      // Set Option Satuan Barang
      $('#formTambah #harga_satuan').html('');
      var result = $('#formTambah #id_barang').val();
      $.get('{{ url('barang/show') }}/'+result,function(res1){
        $('#box-keterangan-barang #kode_barang').text(res1.kode_barang);
        $('#box-keterangan-barang #supplier').text(res1.supplier.nama_supplier);
        $('#box-keterangan-barang #jenis_barang').text(res1.jenis_barang.nama_jenis_barang);

        $.get('{{ url('satuan_barang') }}/'+result, function(res2){
          if(res2 != ''){
            $('#formTambah #harga_satuan').removeAttr('disabled');
          }else{
            $('#formTambah #harga_satuan').attr('disabled');
          }
          $.each(res2,function(i,item){
            $('#formTambah #harga_satuan').append(`
              <option value="`+item.id_harga+`.|.`+item.harga+`" >`+item.nama_satuan+` (Rp. `+item.harga+`)</option>
              `);
            });
            $('#formTambah #harga_satuan').select2();

            $('#formTambah #harga_satuan').on('change',function(e){
              setHarga();
            });
          });
      });
      $('#formTambah #harga_satuan').append(`
        <option value="" selected disabled>Pilih Barang</option>
      `);
      $('#formTambah #id_barang').select2();
    }

    $('document').ready(function(){
      $('#tableBarangKeluar').DataTable();

      $('#formTambah #id_barang').select2();

      setSatuanHarga();

      $('#formTambah #id_barang').on('change',function(e){
        setSatuanHarga();
      });
      $('#formTambah #jumlah_barang').on('change input keyup',function(e){
        calcTotalHarga();
      });
    });
  </script>
@endpush
