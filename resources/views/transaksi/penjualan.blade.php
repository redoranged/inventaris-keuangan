@extends('adminlte::page')

@section('title', 'M-Keuangan - Penjualan')

@section('content_header')
  <h1>
    Penjualan
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fas fa-tachometer-alt"></i> Home</a></li>
    <li>Transaksi</li>
    <li class="active">Penjualan</li>
  </ol>
@stop

@section('content')
  {{-- Start Table Penjualan --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Penjualan <span class="badge"></span></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-bordered" id="tablePenjualan">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Aksi</th>
                    <th>Total Harga</th>
                    <th>User</th>
                    <th>Tanggal Transaksi</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($penjualan as $item)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                      <button type="button" data-transaksi="{{ $item->transaksi }}" class="btn btn-sm btn-warning btn-detail-penjualan"> DETAIL <i class="fas fa-list"></i></button>
                    </td>
                    <td>Rp. {{ number_format($item->total_harga,0,",",".") }}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ date('d/M/Y',strtotime($item->tanggal_transaksi)) }}<br>{{ date('h:i A',strtotime($item->tanggal_transaksi)) }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Table Penjualan --}}

  {{-- Start Form Tambah --}}
  <div class="row">
    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Penjualan</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /. box-header -->

        <div class="box-body">
          <div class="container-fluid">

            <form action="{{ route('penjualan.store') }}" id="formTambah" method="post">
              @csrf
              @if (Auth::user()->role == 'admin')
                <div class="row">
                  <div class="col-md-offset-2 col-md-8 col-xs-12">
                    <div class="form-group">
                      <label for="id_perusahaan">Perusahaan</label>
                      <select name="" id="id_perusahaan_select" class="form-control select2">
                        @foreach ($perusahaan as $item)
                            <option value="{{ $item->id_perusahaan }}|{{ $item->saldo }}">{{ $item->nama_perusahaan }} (Rp. {{ number_format($item->saldo,2,',','.') }})</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              @endif
              <input type="hidden" name="id_perusahaan" id="id_perusahaan" value="{{ (Auth::user()->role == 'user') ? Auth::user()->id_perusahaan : '' }}">
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                  <div class="form-group">
                    <label for="nama_pembeli">Nama Pembeli</label>
                    <input type="text" class="form-control" id="nama_pembeli" name="nama_pembeli" placeholder="Nama Pembeli" required/>
                    @if ($errors->has('nama_pembeli'))
                      <span class="help-block">
                        <strong>{{ $errors->first('nama_pembeli') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="kontak_pembeli">Kontak Pembeli</label>
                    <input type="text" class="form-control" id="kontak_pembeli" name="kontak_pembeli" placeholder="Kontak Pembeli" required/>
                    @if ($errors->has('kontak_pembeli'))
                      <span class="help-block">
                        <strong>{{ $errors->first('kontak_pembeli') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-offset-2 col-md-8 col-xs-12">
                  <div class="form-group">
                    <label for="tanggal_transaksi">Tanggal Transaksi</label>
                    <input type="text" name="tanggal_transaksi" id="tanggal_transaksi" class="form-control" data-type="daterangepicker">
                    @if ($errors->has('tanggal_transaksi'))
                      <span class="help-block">
                        <strong>{{ $errors->first('tanggal_transaksi') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-hover table-bordered" id="tableTambahPenjualan">
                    <thead>
                      <tr>
                        <th>Barang</th>
                        <th>Satuan</th>
                        <th>Jumlah Barang</th>
                        <th>Harga</th>
                        <th>
                          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalTambahBarangRow" id="addNewRowItem"><i class="fas fa-plus"></i></button>
                        </th>
                      </tr>
                    </thead>
                    <tbody id="rowTambahPenjualan">

                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-md-offset-2 col-md-8">
                  <div class="form-group">
                    <label for="total_harga">Total Harga</label>
                    <div class="input-group">
                      <span class="input-group-addon">Rp.</span>
                      <input type="text" class="form-control" id="total_harga" name="jumlah_harga" data-mask="true" value="0" min-value="0" placeholder="Total Harga" readonly required/>
                    </div>
                    <span class="help-block" id="saldo_lebih" style="display:none;">
                      <strong class="text-danger">Saldo tidak cukup!</strong>
                    </span>
                    @if ($errors->has('total_harga'))
                      <span class="help-block">
                        <strong>{{ $errors->first('total_harga') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div style="float:right;">
                  <button type="submit" class="btn btn-success" id="btnSubmit">
                    Beli
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- End Form Tambah --}}

  {{-- Start Modal Tambah --}}
    <div class="modal fade" id="modalTambahBarangRow" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="modal-title">Tambah Barang Penjualan</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="id_barang">Nama Barang</label>
              <select class="form-control select2" id="id_barang" name="id_barang" style="width:100%;">
                <option value="" selected disabled>Pilih Barang</option>
              @foreach($barang as $item)
                <option value="{{ $item->id_barang }}" id="harga_satuan_{{ $item->id_barang }}">{{ $item->nama_barang }}</option>
              @endforeach
              </select>
              <input type="hidden" name="nama_barang" id="nama_barang" />
            </div>
            <div id="box-keterangan-barang" class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tentang Barang</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                      <label class="col-md-5" for="kode_barang">Kode Barang </label>
                      <span class="col-md-1">:</span>
                      <span class="col-md-6" id="kode_barang"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                      <label class="col-md-5" for="jenis_barang">Jenis Barang </label>
                      <span class="col-md-1">:</span>
                      <span class="col-md-6" id="jenis_barang"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                      <label class="col-md-5" for="supplier">Supplier </label>
                      <span class="col-md-1">:</span>
                      <span class="col-md-6" id="supplier"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="harga_satuan">Harga Satuan Barang</label>
              <select class="form-control select2" id="harga_satuan" name="harga_satuan" style="width:100%;" disabled>
                <option value="" selected disabled>Pilih Barang</option>
              </select>
              <input type="hidden" name="id_harga" id="id_harga" value="0">
              <input type="hidden" name="harga" id="harga" value="0"/>
            </div>
            <div class="form-group">
              <label for="jumlah_barang">Jumlah Barang</label>
              <input type="text" name="jumlah_barang" id="jumlah_barang" class="form-control" placeholder="Jumlah Barang" data-mask="true" value="0" min-value="0" required/>
            </div>
            <div class="form-group">
              <label for="total_harga">Total Harga</label>
              <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="text" name="total_harga" id="total_harga" class="form-control" placeholder="Total Harga" data-mask="true" value="0" min-value="0" readonly required/>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnBatalTambahRow">BATAL</button>
            <button type="button" class="btn btn-primary" id="btnTambahRow">TAMBAH</button>
          </div>
        </div>
      </div>
    </div>
  {{-- End Modal Tambah --}}

  {{-- Start Modal Detail --}}
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="modal-title">Detail Penjualan</h4>
          </div>
          <div class="modal-body">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Jumlah Barang/Satuan</th>
                  <th>Total Harga</th>
                </tr>
              </thead>
              <tbody id="rowModalDetail">

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  {{-- End Modal Detail --}}
@stop

@push('js')
  <script type="text/javascript">
    function calcTotalHarga(){
      var harga = $('#modalTambahBarangRow #harga').val();
      var jumlah_barang = $('#modalTambahBarangRow #jumlah_barang').val();

      $('#modalTambahBarangRow #total_harga').val(harga*jumlah_barang);

      $('#modalTambahBarangRow #total_harga').inputmask();
    }

    function setHarga(){

      var result = $('#modalTambahBarangRow #harga_satuan').val();
      var value = result.split(".|.");

      $('#modalTambahBarangRow #id_harga').val(value[0]);
      $('#modalTambahBarangRow #harga').val(value[1]);
      calcTotalHarga();
    }

    function setSatuanHarga(){
      // Set Option Satuan Barang
      $('#modalTambahBarangRow #harga_satuan').html('');
      var result = $('#modalTambahBarangRow #id_barang').val();
      $.get('{{ url('barang/show') }}/'+result,function(res1){
        $('#box-keterangan-barang #kode_barang').text(res1.kode_barang);
        $('#box-keterangan-barang #supplier').text(res1.supplier.nama_supplier);
        $('#box-keterangan-barang #jenis_barang').text(res1.jenis_barang.nama_jenis_barang);
        $('#modalTambahBarangRow #nama_barang').val(res1.nama_barang);

        $.get('{{ url('satuan_barang') }}/'+result, function(res2){
          if(res2 != ''){
            $('#modalTambahBarangRow #harga_satuan').removeAttr('disabled');
          }else{
            $('#modalTambahBarangRow #harga_satuan').attr('disabled');
          }
          $.each(res2,function(i,item){
            $('#modalTambahBarangRow #harga_satuan').append(`
              <option value="`+item.id_harga+`.|.`+item.harga+`.|.`+item.nama_satuan+`" >`+item.nama_satuan+` (Rp. `+item.harga+`)</option>
              `);
            });
            $('#modalTambahBarangRow #harga_satuan').select2();

            $('#modalTambahBarangRow #harga_satuan').on('change',function(e){
              setHarga();
            });
          });
      });
      $('#modalTambahBarangRow #harga_satuan').append(`
        <option value="" selected disabled>Pilih Barang</option>
      `);
      $('#modalTambahBarangRow #id_barang').select2();
    }

    function calculateTotalHarga(){
      var sum = 0;
      $(".total_harga_row_barang").each(function(){
          sum += +$(this).val();
      });
      $("#formTambah #total_harga").val(sum);
    }

    function validateSaldo(){
      var value = $('#id_perusahaan_select').val().split('|');
      $('#id_perusahaan').val(value[0]);
    }

    $('document').ready(function(){

      $('#id_perusahaan_select').on('change',function(){
        validateSaldo();
      });

      calculateTotalHarga();
      validateSaldo();
      // SALDO VALIDATION
      $('#formTambah #total_harga').on('change keyup each',function(e){
        validateSaldo();
      });

      //DETAIL
      $('.btn-detail-penjualan').click(function(e){
        $('#rowModalDetail').html('');
        var transaksi = $(this).data('transaksi');

        var total_harga = 0;

        $.each(transaksi,function(i,item){
          total_harga += item.total_harga;
          $('#rowModalDetail').append(`
            <tr>
              <td>`+(i+1)+`</td>
              <td>`+item.harga.barang.nama_barang+`</td>
              <td>`+item.jumlah_barang+` /`+item.harga.nama_satuan+`</td>
              <td> Rp. `+item.total_harga+`</td>
            </tr>
          `);
        });

        $('#rowModalDetail').append(`
          <tr>
            <th colspan="3">Total Harga</th>
            <td> Rp. `+total_harga+`</td>
          </tr>
        `);

        $('#modalDetail').modal('show');
        validateSaldo();
      });

      var numberRow = 1;
      $('#btnTambahRow').click(function(e){
        var barang        = $('#modalTambahBarangRow #nama_barang').val();
        var harga_satuan  = $('#modalTambahBarangRow #harga_satuan').val();
        var id_harga      = harga_satuan.split('.|.')[0];
        var harga         = harga_satuan.split('.|.')[1];
        var nama_satuan   = harga_satuan.split('.|.')[2];
        var jumlah_barang = $('#modalTambahBarangRow #jumlah_barang').val();
        var total_harga   = $('#modalTambahBarangRow #total_harga').val();

        $('#rowTambahPenjualan').append(`
          <tr>
            <td>
              <span id="nama_barang`+numberRow+`">`+barang+`</span>
            </td>
            <td>
              <input type="hidden" name="id_harga[]" id="id_harga`+numberRow+`" value="`+id_harga+`">
              <span id="harga_satuan`+numberRow+`">`+nama_satuan+` (Rp. `+harga+`)</span>
            </td>
            <td>
              <input type="text" name="jumlah_barang[]" data-text="true" id="jumlah_barang`+numberRow+`" class="form-control" placeholder="Jumlah Barang" data-mask="true" value="`+jumlah_barang+`" min-value="0" required readonly/>
            </td>
            <td>
              <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="text" name="total_harga[]" id="total_harga`+numberRow+`" class="form-control total_harga_row_barang" placeholder="Total Harga" data-mask="true" value="`+total_harga+`" min-value="0" readonly required/>
              </div>
            </td>
            <td>
              <button type="button" class="btn btn-danger btnRemoveItem"><i class="fas fa-minus"></i></button>
            </td>
          </tr>
        `);

        $('[data-mask=true]').inputmask({
          alias:"numeric",
          digits:0,
          digitsOptional:false,
          decimalProtect:true,
          groupSeparator:".",
          radixPoint:",",
          radixFocus:true,
          autoGroup:true,
          autoUnmask:true,
          removeMaskOnSubmit:true
        });

        numberRow++;

        calculateTotalHarga();

        $('.btnRemoveItem').click(function(e){
            if ($(this).closest('tbody').children().length == 1) {
              swal('Warning!','Minimal harus ada 1 Barang!','warning');
            }else{
              $(this).closest('tr').remove();
            }
            calculateTotalHarga();
            validateSaldo();
        });

        $('#modalTambahBarangRow').modal('hide');
        validateSaldo();
      });

      $('#modalTambahBarangRow #id_barang').select2();

      setSatuanHarga();
      validateSaldo();

      $('#modalTambahBarangRow #id_barang').on('change',function(e){
        validateSaldo();
        setSatuanHarga();
      });
      $('#modalTambahBarangRow #jumlah_barang').on('change input keyup',function(e){
        validateSaldo();
        calcTotalHarga();
      });


      $('.select2').select2();

      $('#tablePenjualan').DataTable();

      // Add Row Item
      // KURANG ROW
      $('.btnRemoveItem').click(function(e){
        if ($(this).closest('tbody').children().length == 1) {
          swal('Warning!','Minimal harus ada 1 Barang!','warning');
        }else{
          $(this).closest('tr').remove();
        }
        validateSaldo();
      });
    });
  </script>
@endpush
