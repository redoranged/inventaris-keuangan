@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.register_message') }}</p>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('kode_perusahaan') ? 'has-error' : '' }}">
                    <div class="input-group">
                      <input type="text" name="kode_perusahaan" id="kode_perusahaan" class="form-control" value="{{ old('kode_perusahaan') }}"
                      placeholder="Kode Perusahaan">
                      <input type="hidden" name="id_perusahaan" id="id_perusahaan" value="">
                      <span class="input-group-btn">
                        <button type="button" class="btn btn-success" id="cekKodePerusahaan">Cek</button>
                      </span>
                    </div>
                    @if ($errors->has('kode_perusahaan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kode_perusahaan') }}</strong>
                        </span>
                    @endif
                </div>

                <div id="registerLogin" style="display:none;">
                    <a href="#" data-toggle="modal" data-target="#modalDetailPerusahaan">Detail Perusahaan</a>
                    <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                               placeholder="{{ trans('adminlte::adminlte.full_name') }}">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input type="password" name="password" class="form-control"
                               placeholder="{{ trans('adminlte::adminlte.password') }}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <input type="password" name="password_confirmation" class="form-control"
                               placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit"
                            class="btn btn-primary btn-block btn-flat"
                    >{{ trans('adminlte::adminlte.register') }}</button>
                </div>
            </form>
            <div class="auth-links">
                <a href="{{ url(config('adminlte.login_url', 'login')) }}"
                   class="text-center">{{ trans('adminlte::adminlte.i_already_have_a_membership') }}</a>
            </div>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->

    {{-- Start Modal Detail Perusahaan --}}
      <div class="modal fade" id="modalDetailPerusahaan" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="titleNamaPerusahaan"></h4>
            </div>
            <div class="modal-body">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                    <center>
                      <img id="logo_perusahaan" style="width:250px;display:block;">
                    </center>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="nama_perusahaan">Nama Perusahaan</label>
                      <p id="nama_perusahaan"></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="alamat_perusahaan">Alamat Perusahaan</label>
                      <p id="alamat_perusahaan"></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="no_telepon_perusahaan">No Telepon</label>
                      <p id="no_telepon_perusahaan"></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="email_perusahaan">Email Perusahaan</label>
                      <p id="email_perusahaan"></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    {{-- End Modal Detail Perusahaan --}}
@stop

@section('adminlte_js')
    <script type="text/javascript">
      $(document).ready(function(){
        $('#cekKodePerusahaan').click(function(e){
          $.get('{{ url("cek/kode_perusahaan") }}/'+$('#kode_perusahaan').val(),function(res){
            if(JSON.stringify(res) != '{}'){
              $('#registerLogin').fadeIn();
              $('#id_perusahaan').val(res.id_perusahaan);

              // Insert into modal detail
              $('#modalDetailPerusahaan #titleNamaPerusahaan').text(res.nama_perusahaan);
              $('#modalDetailPerusahaan #nama_perusahaan').text(res.nama_perusahaan);
              $('#modalDetailPerusahaan #alamat_perusahaan').text(res.alamat);
              $('#modalDetailPerusahaan #no_telepon_perusahaan').text(res.no_telepon);
              $('#modalDetailPerusahaan #email_perusahaan').text(res.email);

              $('#modalDetailPerusahaan #logo_perusahaan').attr('src','{{ asset("storage/logo-perusahaan") }}/'+res.logo);

              swal('Berhasil!','Kode Perusahaan Dapat digunakan!','success');
            }else{
              $('#registerLogin').fadeOut();
              $('#id_perusahaan').val('');

              // Remove from modal detail
              $('#modalDetailPerusahaan #titleNamaPerusahaan').text('');
              $('#modalDetailPerusahaan #nama_perusahaan').text('');
              $('#modalDetailPerusahaan #alamat_perusahaan').text('');
              $('#modalDetailPerusahaan #no_telepon_perusahaan').text('');
              $('#modalDetailPerusahaan #email_perusahaan').text('');

              $('#modalDetailPerusahaan #logo_perusahaan').removeAttr('src');

              swal('Gagal!','Kode Perusahaan tidak dapat digunakan!','error');
            }
          });
        });
      });
    </script>
    @yield('js')
@stop
