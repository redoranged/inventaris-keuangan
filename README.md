# M-Keuangan

Untuk Inventaris & Keuangan

1. [Clone Repository](#1-clone-repository)
2. [Installation](#2-installation)
3. [Configuration](#3-configuration)
4. [Contributors](#4-contributors)

## 1. Clone Repository

```bash
# with git cli
git clone https://gitlab.com/DnAomissions/inventaris-keuangan.git
# or you can clone from git desktop / git gui
```

## 2. Configuration

### 1. Install Vendor

```bash
# open the command prompt & open your project location
cd /%PATH%/inventaris-keuangan

# install vendor with composer
composer install
```

### 2. Configure ***.env***

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=yourdbname
DB_USERNAME=username
DB_PASSWORD=password

#if you want recycle bin
APP_RECYCLE_BIN=true

#if you want mobile mode
APP_MOBILE_MODE=true
```

### 3. Migrate Database

```bash
# migrate database
php artisan migrate
# if table is exists you drop & remigrate
php artisan migrate:fresh
```

### 4. Publish AdminLTE

```bash
php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets
```
### 5. Create Link from **public/storage** to **storage/app/public**

```bash
php artisan storage:link
```

## 3. Contributors

* **Demistran Team**

See also the list of [contributors](https://gitlab.com/DnAomissions/inventaris-keuangan/graphs/master) who participated in this project.

***

_Copyright @2018, [CV Demistran](https://gitlab.com/DnAomissions)._
