<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Plugin
use Hash;

// Models
use App\Models\User;


class MakeAdmin extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'make:admin { --password= } { --email= } { --name= }';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create admin user | use --password=? for password, --email=? for email, --name=? for name';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $name     = $this->option('name') ?: 'admin';
      $email    = $this->option('email') ?: 'admin@admin.com';
      $password = $this->option('password') ?: 'admin';
      User::create(
          [
              'name'      => $name,
              'role'      => 'admin',
              'email'     => $email,
              'password'  => Hash::make($password),
          ]
      );
      $this->info(sprintf('name: %s, password: %s, email: %s', $name, $password, $email));
  }
}
