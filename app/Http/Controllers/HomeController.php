<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\Barang;
use App\Models\Harga;
use App\Models\JenisBarang;
use App\Models\Pegawai;
use App\Models\Perusahaan;
use App\Models\StokBarang;
use App\Models\Supplier;
use App\Models\Transaksi;
use App\Models\User;

// Models Transaksi
use App\Models\Penjualan;
use App\Models\Pembelian;
use App\Models\Pengeluaran;

// History Log
use App\Models\HistoryLog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard for Mobile.
     *
     * @return \Illuminate\Http\Response
     */

     public function indexMobile()
     {
       $history_log = '';
       $pegawai        = '';
       $perusahaan     = '';
       $transaksi      = '';
       $transaksi_today     = '';
       $users          = '';
       $pengeluaran = [];
       $pembelian = [];
       $penjualan = [];

       $pengeluaranToday = '';
       $pembelianToday = '';
       $penjualanToday = '';

       $nama_bulan = [1,2,3,4,5,6,7,8,9,10,11,12];

       $pengeluaranBulanan = [];
       $pembelianBulanan = [];
       $penjualanBulanan = [];

       $banyak_tanggal = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

       if(Auth::user()->role == 'admin'){
         $history_log = HistoryLog::orderBy('updated_at','DESC')->paginate(10);

         $pegawai        = Pegawai::orderBy('updated_at','DESC')->get();
         $perusahaan     = Perusahaan::orderBy('updated_at','DESC')->get();
         $transaksi      = Transaksi::orderBy('updated_at','DESC')->get();
           $transaksi_today     = Transaksi::where('updated_at','LIKE','%'.date('Y-m-d').'%')->orderBy('updated_at')->get();
         $users          = User::orderBy('updated_at','DESC')->get();

         // Start Render For Grafik Transaksi
           // Start Render Per Year
             foreach ($nama_bulan as $key => $value) {
               $pengeluaran[] = count(Pengeluaran::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
               $penjualan[] = count(Penjualan::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
               $pembelian[] = count(Pembelian::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
             }
           // End Render Per Year

           // Start Render Per Month
             for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
               $pengeluaranBulanan[] = count(Pengeluaran::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
               $penjualanBulanan[] = count(Penjualan::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
               $pembelianBulanan[] = count(Pembelian::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             }
           // End Render Per Month

           // Start Render For Today
             $pengeluaranToday = count(Pengeluaran::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             $penjualanToday = count(Penjualan::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             $pembelianToday = count(Pembelian::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
           // End Render For Today
         // End Render For Grafik Transaksi
       }else if(Auth::user()->role == 'user'){
         $history_log = HistoryLog::whereHas('users',function($query){
           $query->where('id_perusahaan',Auth::user()->id_perusahaan);
         })->orderBy('updated_at','DESC')->paginate(10);

         // SELECT all Data Model
         $pegawai        = Pegawai::orderBy('updated_at','DESC')->get();
         $perusahaan     = Perusahaan::orderBy('updated_at','DESC')->get();
         $transaksi      = Transaksi::whereHas('user',function($query){
           $query->where('id_perusahaan',Auth::user()->id_perusahaan);
         })->orderBy('updated_at','DESC')->get();
           $transaksi_today     = Transaksi::where('updated_at','LIKE','%'.date('Y-m-d').'%')->whereHas('user',function($query){
             $query->where('id_perusahaan',Auth::user()->id_perusahaan);
           })->orderBy('updated_at')->get();
         $users          = User::where('id_perusahaan',Auth::user()->id_perusahaan)->orderBy('updated_at','DESC')->get();

         // Start Render For Grafik Transaksi
           // Start Render Per Year
             foreach ($nama_bulan as $key => $value) {
               $pengeluaran[] = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
               $penjualan[] = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
               $pembelian[] = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
             }
           // End Render Per Year

           // Start Render Per Month
             for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
               $pengeluaranBulanan[] = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
               $penjualanBulanan[] = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
               $pembelianBulanan[] = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             }
           // End Render Per Month

           // Start Render For Today
             $pengeluaranToday = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             $penjualanToday = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
             $pembelianToday = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
           // End Render For Today
         // End Render For Grafik Transaksi
       }

       // SELECT all Data Model
       $barang         = Barang::orderBy('updated_at','DESC')->get();
       $harga          = Harga::orderBy('updated_at','DESC')->get();
       $jenis_barang   = JenisBarang::orderBy('updated_at','DESC')->get();
       $stok_barang    = StokBarang::orderBy('updated_at','DESC')->get();
       $supplier       = Supplier::orderBy('updated_at','DESC')->get();

       // return response()->json($pengeluaranBulanan);

       return view('mobile.home',compact(
         'history_log',
         'barang',
         'harga',
         'jenis_barang',
         'pegawai',
         'perusahaan',
         'stok_barang',
         'supplier',
         'transaksi',
           'transaksi_today',
         'users',
         'pengeluaran',
         'pembelian',
         'penjualan',
         'pengeluaranToday',
         'pembelianToday',
         'penjualanToday',
         'pengeluaranBulanan',
         'pembelianBulanan',
         'penjualanBulanan'
       ));
     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $history_log = '';
        $pegawai        = '';
        $perusahaan     = '';
        $transaksi      = '';
        $transaksi_today     = '';
        $users          = '';
        $pengeluaran = [];
        $pembelian = [];
        $penjualan = [];

        $pengeluaranToday = '';
        $pembelianToday = '';
        $penjualanToday = '';

        $nama_bulan = [1,2,3,4,5,6,7,8,9,10,11,12];

        $pengeluaranBulanan = [];
        $pembelianBulanan = [];
        $penjualanBulanan = [];

        $banyak_tanggal = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

        if(Auth::user()->role == 'admin'){
          $history_log = HistoryLog::orderBy('updated_at','DESC')->paginate(10);

          $pegawai        = Pegawai::orderBy('updated_at','DESC')->get();
          $perusahaan     = Perusahaan::orderBy('updated_at','DESC')->get();
          $transaksi      = Transaksi::orderBy('updated_at','DESC')->get();
            $transaksi_today     = Transaksi::where('updated_at','LIKE','%'.date('Y-m-d').'%')->orderBy('updated_at')->get();
          $users          = User::orderBy('updated_at','DESC')->get();

          // Start Render For Grafik Transaksi
            // Start Render Per Year
              foreach ($nama_bulan as $key => $value) {
                $pengeluaran[] = count(Pengeluaran::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
                $penjualan[] = count(Penjualan::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
                $pembelian[] = count(Pembelian::whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
              }
            // End Render Per Year

            // Start Render Per Month
              for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
                $pengeluaranBulanan[] = count(Pengeluaran::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
                $penjualanBulanan[] = count(Penjualan::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
                $pembelianBulanan[] = count(Pembelian::whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              }
            // End Render Per Month

            // Start Render For Today
              $pengeluaranToday = count(Pengeluaran::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              $penjualanToday = count(Penjualan::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              $pembelianToday = count(Pembelian::whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
            // End Render For Today
          // End Render For Grafik Transaksi
        }else if(Auth::user()->role == 'user'){
          $history_log = HistoryLog::whereHas('users',function($query){
            $query->where('id_perusahaan',Auth::user()->id_perusahaan);
          })->orderBy('updated_at','DESC')->paginate(10);

          // SELECT all Data Model
          $pegawai        = Pegawai::orderBy('updated_at','DESC')->get();
          $perusahaan     = Perusahaan::orderBy('updated_at','DESC')->get();
          $transaksi      = Transaksi::whereHas('user',function($query){
            $query->where('id_perusahaan',Auth::user()->id_perusahaan);
          })->orderBy('updated_at','DESC')->get();
            $transaksi_today     = Transaksi::where('updated_at','LIKE','%'.date('Y-m-d').'%')->whereHas('user',function($query){
              $query->where('id_perusahaan',Auth::user()->id_perusahaan);
            })->orderBy('updated_at')->get();
          $users          = User::where('id_perusahaan',Auth::user()->id_perusahaan)->orderBy('updated_at','DESC')->get();

          // Start Render For Grafik Transaksi
            // Start Render Per Year
              foreach ($nama_bulan as $key => $value) {
                $pengeluaran[] = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
                $penjualan[] = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
                $pembelian[] = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereMonth('updated_at',$value)->whereYear('updated_at',date('Y'))->get());
              }
            // End Render Per Year

            // Start Render Per Month
              for($tanggal=1;$tanggal<=$banyak_tanggal;$tanggal++){
                $pengeluaranBulanan[] = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
                $penjualanBulanan[] = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
                $pembelianBulanan[] = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',$tanggal)->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              }
            // End Render Per Month

            // Start Render For Today
              $pengeluaranToday = count(Pengeluaran::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              $penjualanToday = count(Penjualan::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
              $pembelianToday = count(Pembelian::where('id_perusahaan',Auth::user()->id_perusahaan)->whereDay('updated_at',date('d'))->whereMonth('updated_at',date('m'))->whereYear('updated_at',date('Y'))->get());
            // End Render For Today
          // End Render For Grafik Transaksi
        }

        // SELECT all Data Model
        $barang         = Barang::orderBy('updated_at','DESC')->get();
        $harga          = Harga::orderBy('updated_at','DESC')->get();
        $jenis_barang   = JenisBarang::orderBy('updated_at','DESC')->get();
        $stok_barang    = StokBarang::orderBy('updated_at','DESC')->get();
        $supplier       = Supplier::orderBy('updated_at','DESC')->get();

        // return response()->json($pengeluaranBulanan);

        return view('home',compact(
          'history_log',
          'barang',
          'harga',
          'jenis_barang',
          'pegawai',
          'perusahaan',
          'stok_barang',
          'supplier',
          'transaksi',
            'transaksi_today',
          'users',
          'pengeluaran',
          'pembelian',
          'penjualan',
          'pengeluaranToday',
          'pembelianToday',
          'penjualanToday',
          'pengeluaranBulanan',
          'pembelianBulanan',
          'penjualanBulanan'
        ));
    }
}
