<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\Pengeluaran;
use App\Models\Transaksi;

  // Relation
  use App\Models\Barang;
  use App\Models\Harga;
  use App\Models\StokBarang;

  // History Log
  use App\Models\HistoryLog;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengeluaran = '';
        if(Auth::user()->role == 'admin'){
          $pengeluaran = Pengeluaran::orderBy('tanggal_transaksi','DESC')->get();
        }else{
          $pengeluaran = Pengeluaran::where('id_user',Auth::user()->id)->orderBy('tanggal_transaksi','DESC')->get();
        }

        return view('transaksi.pengeluaran',compact('pengeluaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'keperluan'   => 'required',
          'total_harga' => 'required',
          'tanggal_transaksi' => 'required',
        ]);

        $request['id_user'] = Auth::user()->id;
        $request['id_perusahaan'] = Auth::user()->id_perusahaan;
        
        $tanggal_transaksi = date('Y-m-d H:i:s',strtotime($request->tanggal_transaksi));

        $request->tanggal_transaksi = $tanggal_transaksi;

        Pengeluaran::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Pengeluaran</strong>',
          'table'         => 'pengeluaran'
        ]);

        Session::flash('success','Berhasil Tambah Pengeluaran!');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
