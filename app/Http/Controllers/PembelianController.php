<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Auth;
use Session;

// Models
use App\Models\Pembelian;
use App\Models\Transaksi;

  // Relation
  use App\Models\Barang;
  use App\Models\Harga;
  use App\Models\StokBarang;
  use App\Models\Perusahaan;

  // History Log
  use App\Models\HistoryLog;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::orderBy('nama_barang','ASC')->get();

        $pembelian = '';

        if(Auth::user()->role == 'admin'){
          $pembelian = Pembelian::with(['transaksi' => function($query){
                $query->with(['harga' => function($query){
                  $query->with('barang');
                }])
                ->where('jenis','pembelian');
              },'user'])
          ->orderBy('tanggal_transaksi','DESC')->get();
          $perusahaan = Perusahaan::all();
          return view('transaksi.pembelian',compact('barang','pembelian','perusahaan'));
        }else{
          $pembelian = Pembelian::with(['transaksi' => function($query){
                $query->with(['harga' => function($query){
                  $query->with('barang');
                }])
                ->where('jenis','pembelian');
              },'user'])
              ->where('id_user',Auth::user()->id)
              ->orderBy('tanggal_transaksi','DESC')->get();
          return view('transaksi.pembelian',compact('barang','pembelian'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request);
        $request->validate([
          'jumlah_harga'     => 'required',
          'total_harga.*'   => 'required',
          'tanggal_transaksi' => 'required',
        ]);
        
        $tanggal_transaksi = date('Y-m-d H:i:s',strtotime($request->tanggal_transaksi));
        
        $pembelian = Pembelian::create([
          'total_harga' => $request->jumlah_harga,
          'id_perusahaan' => $request->id_perusahaan,
          'id_user' => Auth::user()->id,
          'tanggal_transaksi' => $tanggal_transaksi
        ]);

        foreach($request->id_harga as $index => $id_harga){
          Transaksi::create([
            'id_detail_transaksi'   => $pembelian->id_pembelian,
            'id_harga'              => $id_harga,
            'tipe_transaksi'        => 1,
            'jenis'                 => 'pembelian',
            'id_user'               => Auth::user()->id,
            'jumlah_barang'         => $request->jumlah_barang[$index],
            'total_harga'           => $request->total_harga[$index]
          ]);

          StokBarangController::updateStokBarang($id_harga);
        }

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Transaksi <strong>Pembelian</strong>',
          'table'         => 'pembelian'
        ]);

        Session::flash('success','Berhasil Menambahkan Pembelian!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
