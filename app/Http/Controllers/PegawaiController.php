<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugin
use Auth;
use Session;

// Models
use App\Models\Pegawai;
use App\Models\Perusahaan;

// History Log
use App\Models\HistoryLog;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource for Mobile.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMobile()
    {
        //
        $pegawai = Pegawai::orderBy('updated_at','DESC')->get();
        $pegawai_bin = Pegawai::onlyTrashed()->orderBy('updated_at','DESC')->get();

        $perusahaan = Perusahaan::orderBy('nama_perusahaan','ASC')->get();
        return view('mobile.master.pegawai', compact('pegawai','pegawai_bin', 'perusahaan'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pegawai = Pegawai::orderBy('updated_at','DESC')->get();
        $pegawai_bin = Pegawai::onlyTrashed()->orderBy('updated_at','DESC')->get();

        $perusahaan = Perusahaan::orderBy('nama_perusahaan','ASC')->get();
        return view('master.pegawai', compact('pegawai','pegawai_bin', 'perusahaan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'email'          => 'required',
          'nama_pegawai'   => 'required'
        ]);

        Pegawai::create($request->all());

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'create',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menambahkan Data <strong>Pegawai</strong>',
          'table'         => 'pegawai'
        ]);

        Session::flash('success','Berhasil Tambah Pegawai!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'email'          => 'required',
        'nama_pegawai'   => 'required'
      ]);

      $pegawai = Pegawai::findOrFail($id);

      $pegawai->update($request->all());

      HistoryLog::create([
        'id_user'       => Auth::user()->id,
        'tipe'          => 'update',
        'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengubah Data <strong>Pegawai</strong>',
        'table'         => 'pegawai'
      ]);

      Session::flash('success','Berhasil Update Pegawai!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $pegawai_bin = Pegawai::onlyTrashed()->findOrFail($request->id_pegawai)->restore();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'restore',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah mengembalikan Data <strong>Pegawai</strong>',
          'table'         => 'pegawai'
        ]);

        return response()->json($pegawai_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pegawai = Pegawai::findOrFail(decrypt($id));

        $pegawai->delete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'delete',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah memindahkan Data <strong>Pegawai</strong> ke Recycle Bin',
          'table'         => 'pegawai'
        ]);

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin Pegawai!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $pegawai_bin = Pegawai::onlyTrashed()->findOrFail(decrypt($id))->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroy',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah menghapus Data <strong>Pegawai</strong>',
          'table'         => 'pegawai'
        ]);

        return response()->json($pegawai_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $pegawai_bin = Pegawai::onlyTrashed()->forceDelete();

        HistoryLog::create([
          'id_user'       => Auth::user()->id,
          'tipe'          => 'destroyAll',
          'action'        => '<strong>'.Auth::user()->name.'</strong> telah membersihkan Data Recycle Bin <strong>Pegawai</strong>',
          'table'         => 'pegawai'
        ]);

        return response()->json($pegawai_bin);
    }
}
