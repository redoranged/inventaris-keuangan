<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Plugins
use Session;

// Models
use App\Models\StokBarang;

  use App\Models\Transaksi;

class StokBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stok_barang = StokBarang::orderBy('updated_at','DESC')->get();
        $stok_barang_bin = StokBarang::onlyTrashed()->orderBy('updated_at','DESC')->get();
        return view('master.stok_barang', compact('stok_barang','stok_barang_bin'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
          'kode_stok_barang'   => 'required|unique:stok_barang',
          'nama_stok_barang'   => 'required'
        ]);

        StokBarang::create($request->all());

        Session::flash('success','Berhasil Tambah StokBarang!');

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'kode_stok_barang'   => 'required',
        'nama_stok_barang'   => 'required'
      ]);

      $stok_barang = StokBarang::findOrFail($id);

      $stok_barang->update($request->all());

      Session::flash('success','Berhasil Update StokBarang!');

      return back();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binRestore(Request $request)
    {
        //
        $stok_barang_bin = StokBarang::onlyTrashed()->findOrFail($request->id_stok_barang)->restore();

        return response()->json($stok_barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $stok_barang = StokBarang::findOrFail(decrypt($id));

        $stok_barang->delete();

        Session::flash('success','Berhasil Memindahkan ke Recycle Bin StokBarang!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroy($id)
    {
        //
        $stok_barang_bin = StokBarang::onlyTrashed()->findOrFail(decrypt($id))->forceDelete();

        return response()->json($stok_barang_bin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function binDestroyAll()
    {
        //
        $stok_barang_bin = StokBarang::onlyTrashed()->forceDelete();

        return response()->json($stok_barang_bin);
    }

    public static function updateStokBarang($id_harga){
      $stok_masuk = (int) Transaksi::where('tipe_transaksi','1')->where('id_harga',$id_harga)->sum('jumlah_barang');

      $stok_keluar = (int) Transaksi::where('tipe_transaksi','2')->where('id_harga',$id_harga)->sum('jumlah_barang');

      $total_stok = $stok_masuk - $stok_keluar;

      $stok_barang = StokBarang::where('id_harga',$id_harga)->first();

      $stok_barang->update([
        'stok' => (int) $total_stok
      ]);

      return (int) $total_stok;
    }
}
