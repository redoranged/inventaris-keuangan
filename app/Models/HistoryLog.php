<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistoryLog extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_history_log';

    protected $table = 'history_log';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function users(){
      return $this->belongsTo(User::class,'id_user')->withDefault();
    }
}
