<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_barang';

    protected $table = 'barang';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function supplier(){
      return $this->belongsTo(Supplier::class,'id_supplier')->withDefault();
    }

    public function jenis_barang(){
      return $this->belongsTo(JenisBarang::class,'id_jenis_barang')->withDefault();
    }

    public function harga(){
      return $this->hasMany(Harga::class,'id_barang');
    }
}
