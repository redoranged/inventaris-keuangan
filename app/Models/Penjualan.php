<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penjualan extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_penjualan';

    protected $table = 'penjualan';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function transaksi(){
      return $this->hasMany(Transaksi::class,'id_detail_transaksi','id_penjualan');
    }

    public function user(){
      return $this->belongsTo(User::class,'id_user')->withDefault();
    }
}
