<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_supplier';

    protected $table = 'supplier';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
}
