<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GajiPegawai extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_gaji_pegawai';

    protected $table = 'gaji_pegawai';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function user(){
      return $this->belongsTo(User::class,'id_user')->withDefault();
    }

    public function pegawai(){
      return $this->belongsTo(Pegawai::class,'id_pegawai')->withDefault();
    }

    public function perusahaan(){
      return $this->belongsTo(Perusahaan::class,'id_perusahaan')->withDefault();
    }
}
