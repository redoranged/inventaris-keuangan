<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StokBarang extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_stok_barang';

    protected $table = 'stok_barang';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function harga(){
      return $this->belongsTo(Harga::class,'id_harga')->withDefault();
    }
}
