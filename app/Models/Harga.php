<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Harga extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id_harga';

    protected $table = 'harga';

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function barang(){
      return $this->belongsTo(Barang::class,'id_barang')->withDefault();
    }
}
