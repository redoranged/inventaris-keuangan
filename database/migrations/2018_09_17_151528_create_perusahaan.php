<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->increments('id_perusahaan');
            $table->string('kode_perusahaan')->unique();
            $table->string('nama_perusahaan');
            $table->text('alamat')->nullable();
            $table->string('no_telepon')->nullable();
            $table->bigInteger('saldo')->default(0);
            $table->string('email');
            $table->text('logo');
            $table->string('nama_logo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan');
    }
}
