<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerBeforeUpdateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       DB::unprepared('
       CREATE TRIGGER `m_keuangan_db`.`before_update_transaksi`
        BEFORE UPDATE
        ON `m_keuangan_db`.`transaksi`
        FOR EACH ROW
        BEGIN
          IF old.tipe_transaksi = "1" THEN
            UPDATE stok_barang SET stok = stok-(old.jumlah_barang-new.jumlah_barang) WHERE id_harga = new.id_harga;
          END IF;
          IF old.tipe_transaksi = "2" THEN
            UPDATE stok_barang SET stok = stok+(old.jumlah_barang-new.jumlah_barang) WHERE id_harga = new.id_harga;
          END IF;
        END
       ');
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
          DB::unprepared('DROP TRIGGER `m_keuangan_db`.`before_update_transaksi`');
     }
}
