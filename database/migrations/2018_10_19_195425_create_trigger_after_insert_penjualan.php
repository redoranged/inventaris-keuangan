<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerAfterInsertPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('
      CREATE TRIGGER `m_keuangan_db`.`after_insert_penjualan`
       AFTER INSERT
       ON `m_keuangan_db`.`penjualan`
       FOR EACH ROW
       BEGIN
         UPDATE perusahaan SET saldo = saldo+new.total_harga WHERE id_perusahaan = new.id_perusahaan;

         INSERT INTO history_log
         VALUE(
            null,
            new.id_user,
            "create",
            CONCAT("<strong>",(SELECT name FROM users WHERE id = new.id_user LIMIT 1),"</strong> telah menambahkan Data <strong>Penjualan</strong>"),
            "penjualan",
            new.created_at,
            new.updated_at,
            null
         );
       END
      ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `m_keuangan_db`.`after_insert_penjualan`');
    }
}
