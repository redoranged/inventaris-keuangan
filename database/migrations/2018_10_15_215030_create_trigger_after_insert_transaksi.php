<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerAfterInsertTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('
        CREATE TRIGGER `m_keuangan_db`.`after_insert_transaksi`
          AFTER INSERT
          ON `m_keuangan_db`.`transaksi`
          FOR EACH ROW
          BEGIN
    	       IF new.tipe_transaksi = "1" THEN
    		        UPDATE stok_barang SET stok = stok+new.jumlah_barang WHERE id_harga = new.id_harga;
             END IF;
  	         IF new.tipe_transaksi = "2" THEN
    		        UPDATE stok_barang SET stok = stok-new.jumlah_barang WHERE id_harga = new.id_harga;
             END IF;
          END
      ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::unprepared('DROP TRIGGER `m_keuangan_db`.`after_insert_transaksi`');
    }
}
