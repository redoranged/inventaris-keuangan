<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengeluaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('pengeluaran', function (Blueprint $table) {
           $table->increments('id_pengeluaran');
           $table->text('keperluan');
           $table->integer('id_user'); //Reference 'id_user' -> table 'user'
           $table->integer('id_perusahaan');
           $table->bigInteger('total_harga')->default(0);
           $table->timestamp('tanggal_transaksi')->default(DB::raw('CURRENT_TIMESTAMP'));
           $table->timestamps();
           $table->softDeletes();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('pengeluaran');
     }
}
