<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('transaksi', function (Blueprint $table) {
             $table->increments('id_transaksi');
             $table->integer('id_detail_transaksi');
             $table->integer('id_harga'); //Reference 'id_harga' -> table 'harga'
             $table->string('tipe_transaksi')->default(1);
             $table->string('jenis')->nullable();
             $table->integer('id_user'); //Reference 'id_user' -> table 'user'
             $table->bigInteger('jumlah_barang')->default(0);
             $table->bigInteger('total_harga')->default(0);
             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('transaksi');
     }
}
