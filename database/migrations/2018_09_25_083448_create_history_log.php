<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('history_log', function (Blueprint $table) {
             $table->increments('id_history_log');
             $table->integer('id_user'); //Reference 'id_user' -> table 'user'
             $table->string('tipe')->nullable();
             $table->text('action')->nullable();
             $table->string('table')->nullable();
             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('history_log');
     }
}
