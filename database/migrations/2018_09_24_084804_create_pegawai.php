<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('pegawai', function (Blueprint $table) {
             $table->increments('id_pegawai');
             $table->string('nama_pegawai');
             $table->integer('id_perusahaan'); //Reference 'id_perusahaan' -> table 'perusahaan'
             $table->string('email')->nullable();
             $table->string('no_telepon');
             $table->text('alamat');
             $table->text('keterangan')->nullable();
             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('pegawai');
     }
}
