<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker\Factory::create('id_ID');

      $name     = 'admin';
      $email    = 'admin@admin.com';
      $password = 'admin';
      DB::table('users')->insert(
          [
              'name'      => $name,
              'role'      => 'admin',
              'email'     => $email,
              'password'  => Hash::make($password),
          ]
      );

      foreach (range(1, 20) as $loop) {
        DB::table('users')->insert([
            'name'          => $faker->name,
            'email'         => $faker->freeEmail,
            'id_perusahaan' => rand(1,20),
            'password'      => Hash::make('mkeuangan'),
            'created_at'        => $faker->dateTime,
            'updated_at'        => $faker->dateTime,
        ]);

        DB::table('supplier')->insert([
            'kode_supplier'     => 'S'.str_random(5),
            'nama_supplier'     => $faker->company,
            'alamat'            => $faker->address,
            'kontak'            => $faker->freeEmail,
            'created_at'        => $faker->dateTime,
            'updated_at'        => $faker->dateTime,
        ]);

        DB::table('jenis_barang')->insert([
            'kode_jenis_barang' => 'JB'.str_random(4),
            'nama_jenis_barang' => $faker->firstName,
            'created_at'        => $faker->dateTime,
            'updated_at'        => $faker->dateTime,
        ]);

        DB::table('perusahaan')->insert([
            'kode_perusahaan'   => 'P'.str_random(5),
            'nama_perusahaan'   => $faker->company,
            'alamat'            => $faker->address,
            'no_telepon'        => $faker->phoneNumber,
            'saldo'             => rand(1,100000)*1000,
            'email'             => $faker->companyEmail,
            'logo'              => $faker->imageUrl,
            'nama_logo'         => 'Example',
            'created_at'        => $faker->dateTime,
            'updated_at'        => $faker->dateTime,
        ]);

        DB::table('pengeluaran')->insert([
            'keperluan'         => $faker->paragraph,
            'id_user'           => rand(2,20),
            'id_perusahaan'     => rand(1,20),
            'total_harga'       => rand(1,1000)*1000,
            'created_at'        => $faker->dateTime,
            'updated_at'        => $faker->dateTime,
        ]);
      }
    }
}
